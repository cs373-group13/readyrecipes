import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.chrome.options import Options
from time import sleep

options = Options()
options.add_argument('--window-size=1920,1080')
options.add_argument('--disable-dev-shm-usage')
options.add_argument("--no-sandbox")
options.headless = True
driver = webdriver.Chrome(options=options)
delay = 50

class Selenium(unittest.TestCase):

    #Tests if home page correctly loads
    def test1(self):
        driver.get('https://www.readyrecipes.me')
        driver.implicitly_wait(delay)
        result = driver.find_elements_by_class_name('center')
        self.assertNotEqual(result, None)

    #Tests if the navbar has the correct amount of elements
    def test2(self):
        driver.get('https://www.readyrecipes.me')
        driver.implicitly_wait(delay)
        result = driver.find_elements_by_class_name("NavItem")
        self.assertEqual(len(result), 8)

    #Tests that all 5 people cards loaded into the about page
    def test3(self):
        driver.get('https://www.readyrecipes.me/about')
        driver.implicitly_wait(delay)
        result = driver.find_elements_by_class_name('ordering')
        self.assertEqual(len(result), 5)
    
    #Test clicking geolocation
    def test4(self):
        driver.get('https://www.readyrecipes.me/geolocation')
        driver.implicitly_wait(delay)
        result = driver.find_element_by_xpath("//*[@id=\"root\"]/div[2]/div/div/div[3]/table/tbody/tr[1]/td[1]/p")
        result.click()
        self.assertEqual(driver.current_url, 'https://www.readyrecipes.me/geolocationInstance/1283240')

    #Test clicking recipe
    def test5(self):
        driver.get('https://www.readyrecipes.me/recipes')
        driver.implicitly_wait(delay)
        result = driver.find_element_by_xpath("//*[@id=\"root\"]/div[2]/div[2]/div/div/div[3]/table/tbody/tr[1]/td[1]/p/span/span")
        result.click()
        self.assertEqual(driver.current_url, 'https://www.readyrecipes.me/recipeInstance/592479')

    #Tests the back button works for recipes items
    def test6(self):
        driver.get('https://www.readyrecipes.me/recipeInstance/592479')
        driver.implicitly_wait(delay)
        result = driver.find_element_by_link_text("Back")
        result.click()
        self.assertEqual(driver.current_url, 'https://www.readyrecipes.me/recipes')
    
    #Tests clicking relation
    def test7(self):
        driver.get('https://readyrecipes.me/gardenInstance/164571')
        driver.implicitly_wait(delay)
        result = driver.find_element_by_xpath("//*[@id=\"root\"]/div[2]/div/div[3]/div[2]/div/div[1]/a/p")
        result.click()
        driver.implicitly_wait(delay)
        self.assertEqual(driver.current_url, 'https://readyrecipes.me/recipeInstance/592479')

if __name__ == '__main__':
    unittest.main()