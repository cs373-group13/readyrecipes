from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from flask import Flask, request
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from shapely.geometry import shape, GeometryCollection, Point
import requests, json, re

from init import db, ma
from models import (
    Recipe,
    RecipeSchema,
    GardenCrop,
    GardenCropSchema,
    Geolocation,
    GeolocationSchema,
    AssocGeoGarden,
)

GEO_API_KEY = "f35080952dab720b3c479d5c0aee3aed"
GEO_BULK_FILE = "./dataMunging/data/weather_14.json"

# Removes spaces and hyphens and lowercases a name
def parse_crop_name(name):
    name = name.replace("-", " ")
    name = name.replace(" ", "")
    name = name = name.lower()
    return name


# Scrapes recipe, iterates over ingredients, and searches for them on trefle.
# Then scrapes that trefle and attach it to recipe
def scrape_recipes_and_gardencrops(num_recipes):
    print("Scraping recipes...")

    recipe_url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/complexSearch"
    querystring = {
        "addRecipeNutrition": "true",
        "addRecipeInformation": "true",
        "number": str(num_recipes),
    }
    headers = {
        "x-rapidapi-key": "a357ebfd0bmsh1eeac8d371ac1f6p17470djsn65a63414b4f8",
        "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
    }

    raw_data = requests.get(url=recipe_url, headers=headers, params=querystring).json()

    print("Got raw data")

    recipes = []
    # Maps ingredient name to list of gardencrop names
    ingredients = dict()
    # Maps gardencrop name to GardenCrop object
    gardencrops = dict()

    for r in raw_data["results"]:

        curr_relations = set()

        # Scrape recipe from r
        robj = scrape_recipe(r)

        for i in robj.ingredients:
            iname = i["name"]

            # If we've searched this ingredient before add all its results to our association table
            if iname in ingredients.keys():
                for crop in ingredients[iname]:
                    if crop not in curr_relations:
                        robj.gardencrops.append(gardencrops[crop])
                        curr_relations.add(crop)

            # If we haven't, search for it
            else:
                crop_url = (
                    "https://trefle.io/api/v1/plants/search?token=pDRsoMz9tw50VktbJ6e4ZJ5olQsMoOcxy668xJlLe_4&q="
                    + iname
                )
                crop_data = requests.get(crop_url).json()

                ingredients[iname] = []

                for c in crop_data["data"]:
                    cname = c["common_name"]
                    if cname and parse_crop_name(iname) in parse_crop_name(cname):
                        # Add to ingredient mapping
                        ingredients[iname].append(cname)

                        # If we haven't seen this crop before scrape it and update gardencrop dict
                        if cname not in gardencrops.keys():
                            print(
                                "Crop " + str(len(gardencrops.values())) + ": " + cname
                            )
                            gobj = scrape_gardencrop(c)
                            gardencrops[cname] = gobj

                        # Add crop to recipe relation if it's not already a relation
                        if cname not in curr_relations:
                            robj.gardencrops.append(gardencrops[cname])
                            curr_relations.add(cname)

        print("Recipe " + str(len(recipes)) + ": " + r["title"])
        recipes.append(robj)

    print(
        "Scraped "
        + str(len(recipes))
        + " recipes and "
        + str(len(gardencrops))
        + " gardencrops"
    )
    print("Pushing recipes and gardencrops...")
    for r in recipes:
        db.session.add(r)
    for g in gardencrops.values():
        db.session.add(g)

    db.session.commit()

    print("Pushed recipes and gardencrops")


# Takes in JSON data and creates a Recipe object
def scrape_recipe(data):
    recipe = dict()
    for field in RecipeSchema.Meta.fields:
        if field in data.keys():
            recipe[field] = data[field]
    # Add ingredients
    recipe["ingredients"] = data["nutrition"]["ingredients"]
    # Remove HTML from summaries
    if "summary" in data.keys():
        recipe["summary"] = removeHTML(data["summary"])

    robj = Recipe(**recipe)
    return robj


def scrape_gardencrop(data):
    token = "token=pDRsoMz9tw50VktbJ6e4ZJ5olQsMoOcxy668xJlLe_4"
    plant = data["links"]["self"]
    plantURL = "https://trefle.io" + plant + "?" + token
    plant_data = requests.get(url=plantURL).json()
    r = plant_data["data"]

    g = dict()
    g["id"] = r["id"]
    g["common_name"] = r["common_name"]
    g["family_common_name"] = r["family_common_name"]
    g["image_url"] = r["image_url"]
    g["genus"] = r["genus"]
    g["family"] = r["family"]
    g["year"] = r["year"]
    g["days_to_harvest"] = r["growth"]["days_to_harvest"]
    g["description"] = r["growth"]["description"]
    g["sowing"] = r["growth"]["sowing"]
    g["light"] = r["growth"]["light"]
    g["minimum_temperature"] = r["growth"]["minimum_temperature"]["deg_f"]
    g["maximum_temperature"] = r["growth"]["maximum_temperature"]["deg_f"]
    g["growth_months"] = r["growth"]["growth_months"]
    g["bloom_months"] = r["growth"]["bloom_months"]

    gobj = GardenCrop(**g)
    return gobj


# Removes any HTML tags from text
def removeHTML(text):
    return re.sub(r"<.*?>", "", text)


# Gets L2 name from a set of coordinates
def coord_in_shape(js, point):
    for feature in js["features"]:
        polygon = shape(feature["geometry"])
        if polygon.contains(point):
            return feature["properties"]["LEVEL2_NAM"]
    return None


def scrape_geolocations():
    print("Scraping geolocations...")
    geolocations = []
    # Open JSON file containing city information
    with open(GEO_BULK_FILE) as f:
        print("Opened geolocation file")

        with open("./dataMunging/level2.geojson", "r") as geo_file:
            print("Opened coord to L2 file")
            coords_to_l2 = json.load(geo_file)
            lines = f.readlines()
            for l in lines:
                # Convert string to JSON object
                r = json.loads(l)

                # NOTE: This just gets US Cities its just temporary.
                if r["city"]["country"] == "US":
                    # Fill in fields
                    g = dict()
                    g["id"] = r["city"]["id"]
                    g["name"] = r["city"]["name"]
                    g["lon"] = r["city"]["coord"]["lon"]
                    g["lat"] = r["city"]["coord"]["lat"]
                    g["weather"] = r["weather"]
                    g["temp"] = r["main"]["temp"]
                    g["temp_min"] = r["main"]["temp_min"]
                    g["temp_max"] = r["main"]["temp_max"]
                    g["pressure"] = r["main"]["pressure"]
                    g["humidity"] = r["main"]["humidity"]
                    g["country"] = r["city"]["country"]

                    # Fill in L2 code
                    g["l2_name"] = coord_in_shape(
                        coords_to_l2,
                        Point(r["city"]["coord"]["lon"], r["city"]["coord"]["lat"]),
                    )

                    geolocations.append(g)
            print("Formatted data")
    return geolocations


def push_geolocations(scraped_data):
    print("Pushing geolocations to database...")
    for d in scraped_data:
        # Convert to model type
        geolocation = Geolocation(**d)
        db.session.add(geolocation)
    db.session.commit()
    print("Pushed geolocations")


def create_geo_garden_assoc():
    print("Creating geolocation/gardencrop association table...")
    with open("./dataMunging/id_to_l2_region.json") as f:
        print("Opened id to l2 region mapping file")
        mapping = json.load(f)
        associations = []
        for cropid in mapping:
            for zone in mapping[cropid]:
                association = {"gardencropid": cropid, "l2_zone": zone}
                associations.append(association)
        print("Mapped all ids to regions")
        print("Pushing associations to database...")
        for a in associations:
            association = AssocGeoGarden(**a)
            db.session.add(association)
        db.session.commit()
        print("Pushed geolocation/gardencrop associations")


if __name__ == "__main__":
    db.create_all()
    scrape_recipes_and_gardencrops(100)
