from unittest import main, TestCase
from shapely.geometry import shape, GeometryCollection, Point
import json

from models import (
    Recipe,
    GardenCrop,
    Geolocation,
    AssocGeoGarden,
    recipe_schema,
    gardencrop_schema,
    geolocation_schema,
)
import dbCreation


class ModelTest(TestCase):
    def test_num_recipes(self):
        self.assertEqual(len(Recipe.query.all()), 100)

    def test_num_gardencrops(self):
        self.assertEqual(len(GardenCrop.query.all()), 497)

    def test_num_geolocations(self):
        self.assertEqual(len(Geolocation.query.all()), 21674)

    def test_schema_recipes(self):
        q = Recipe.query.first()
        r = recipe_schema.dump(q)
        self.assertNotEqual(r["gardencrops"], None)

    def test_schema_gardencrops(self):
        q = GardenCrop.query.first()
        g = gardencrop_schema.dump(q)
        self.assertNotEqual(g["recipes"], None)

    def test_schema_geolocations(self):
        q = Geolocation.query.first()
        g = geolocation_schema.dump(q)
        self.assertNotEqual(g["l2_name"], None)

    def test_num_assocs(self):
        self.assertEqual(len(AssocGeoGarden.query.all()), 226786)

    def test_recipe_crop_assoc(self):
        query = Recipe.query.first()
        self.assertNotEqual(query.gardencrops, None)

    def test_crop_recipe_assoc(self):
        query = GardenCrop.query.first()
        self.assertNotEqual(query.recipes, None)

    def test_l2_names(self):
        query = Geolocation.query.all()
        for q in query:
            self.assertNotEqual(q.l2_name, None)

    def test_recipe_ingredients(self):
        query = Recipe.query.all()
        for q in query:
            self.assertNotEqual(q.ingredients, None)


class CreationTest(TestCase):
    def test_remove_html(self):
        a = "<b>This is bolded</b>"
        b = dbCreation.removeHTML(a)
        self.assertEqual(b, "This is bolded")

    def test_parse_crop_name(self):
        a = "Hello-World !"
        b = dbCreation.parse_crop_name(a)
        self.assertEqual(b, "helloworld!")

    def test_coord_in_shape(self):
        with open("./dataMunging/level2.geojson", "r") as geo_file:
            coords = json.load(geo_file)
            lon = -120.501472
            lat = 47.500118
            p = Point(lon, lat)
            l2 = dbCreation.coord_in_shape(coords, p)
            self.assertEqual(l2, "Northwestern U.S.A.")


if __name__ == "__main__":
    main()
