import requests, json

url = (
    "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/complexSearch"
)
querystring = {
    "addRecipeNutrition": "true",
    "addRecipeInformation": "true",
    "number": "10",
}
headers = {
    "x-rapidapi-key": "a357ebfd0bmsh1eeac8d371ac1f6p17470djsn65a63414b4f8",
    "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
}
# response = requests.request("GET", url, headers=headers, params=querystring)
response = requests.get(url=url, headers=headers, params=querystring)
with open("recipes.json", "w") as outfile:
    outfile.write(response.text)
