from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String

meta = MetaData()
engine = create_engine(
    "postgresql+psycopg2://postgres:readyAWSDB@readyrecipesdb-1.czoahonfctef.us-east-2.rds.amazonaws.com:5432/postgres",
    echo=True,
)

students = Table(
    "students",
    meta,
    Column("id", Integer, primary_key=True),
    Column("name", String),
    Column("lastname", String),
)

meta.create_all(engine)

ins = students.insert()
ins = students.insert().values(name="prudhvi", lastname="varma")
conn = engine.connect()
result = conn.execute(ins)
