from flask import Flask
from flask_sqlalchemy import SQLAlchemy

application = Flask(__name__)

application.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://postgres:readyAWSDB@readyrecipesdb-1.czoahonfctef.us-east-2.rds.amazonaws.com:5432/postgres"
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
application.debug = True

db = SQLAlchemy(application)


class TestScheme(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)


@application.route("/")
def hello_world():
    return "Hello World"


if __name__ == "__main__":
    application.run()
