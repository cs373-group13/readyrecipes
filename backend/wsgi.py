from app import app, db, ma
from models import Recipe, GardenCrop, Geolocation

if __name__ == "__main__":
    app.run(port=8080)
