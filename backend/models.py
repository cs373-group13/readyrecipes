from flask import Flask, json
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from init import db, ma

assoc_recipe_gardencrop = db.Table(
    "assoc_recipe_gardencrop",
    db.Column("recipe_id", db.Integer, db.ForeignKey("recipe.id"), primary_key=True),
    db.Column(
        "gardencrop_id", db.Integer, db.ForeignKey("gardencrop.id"), primary_key=True
    ),
)

class Recipe(db.Model):
    __tablename__ = "recipe"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    sourceUrl = db.Column(db.String)
    image = db.Column(db.String)
    summary = db.Column(db.String)
    servings = db.Column(db.Integer)
    preparationMinutes = db.Column(db.Integer)
    cookingMinutes = db.Column(db.Integer)
    pricePerServing = db.Column(db.Float)
    healthScore = db.Column(db.Integer)
    analyzedInstructions = db.Column(db.JSON)
    ingredients = db.Column(db.JSON)
    gardencrops = db.relationship(
        "GardenCrop", secondary=assoc_recipe_gardencrop, backref=db.backref("recipes")
    )


class GardenCrop(db.Model):
    __tablename__ = "gardencrop"
    id = db.Column(db.Integer, primary_key=True)
    common_name = db.Column(db.String, nullable=True)
    family_common_name = db.Column(db.String, nullable=True)
    image_url = db.Column(db.String, nullable=True)
    genus = db.Column(db.String, nullable=True)
    family = db.Column(db.String, nullable=True)
    year = db.Column(db.Integer, nullable=True)
    days_to_harvest = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String, nullable=True)
    sowing = db.Column(db.String, nullable=True)
    light = db.Column(db.Integer, nullable=True)
    minimum_temperature = db.Column(db.String, nullable=True)
    maximum_temperature = db.Column(db.String, nullable=True)
    growth_months = db.Column(db.ARRAY(db.String), nullable=True)
    bloom_months = db.Column(db.ARRAY(db.String), nullable=True)


class Geolocation(db.Model):
    __tablename__ = "geolocation"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    country = db.Column(db.String)
    lon = db.Column(db.Float)
    lat = db.Column(db.Float)
    temp = db.Column(db.Float)
    temp_min = db.Column(db.Float)
    temp_max = db.Column(db.Float)
    pressure = db.Column(db.Integer)
    humidity = db.Column(db.Integer)
    weather = db.Column(db.JSON)
    l2_name = db.Column(db.String)

class AssocGeoGarden(db.Model):
    __tablename__ = "assocgeogarden"
    id = db.Column(db.Integer, primary_key=True)
    gardencropid = db.Column(db.Integer)
    l2_zone = db.Column(db.String)


class RecipeSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Recipe

    id = ma.auto_field()
    title = ma.auto_field()
    sourceUrl = ma.auto_field()
    image = ma.auto_field()
    summary = ma.auto_field()
    servings = ma.auto_field()
    preparationMinutes = ma.auto_field()
    cookingMinutes = ma.auto_field()
    pricePerServing = ma.auto_field()
    healthScore = ma.auto_field()
    analyzedInstructions = ma.auto_field()
    ingredients = ma.auto_field()
    gardencrops = ma.auto_field()


recipe_schema = RecipeSchema()
recipes_schema = RecipeSchema(many=True)


class GardenCropSchema(ma.SQLAlchemySchema):
    class Meta:
        model = GardenCrop

    id = ma.auto_field()
    common_name = ma.auto_field()
    family_common_name = ma.auto_field()
    image_url = ma.auto_field()
    genus = ma.auto_field()
    family = ma.auto_field()
    year = ma.auto_field()
    days_to_harvest = ma.auto_field()
    description = ma.auto_field()
    sowing = ma.auto_field()
    light = ma.auto_field()
    minimum_temperature = ma.auto_field()
    maximum_temperature = ma.auto_field()
    growth_months = ma.auto_field()
    bloom_months = ma.auto_field()
    recipes = ma.auto_field()


gardencrop_schema = GardenCropSchema()
gardencrops_schema = GardenCropSchema(many=True)


class GeolocationSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Geolocation

    id = ma.auto_field()
    name = ma.auto_field()
    country = ma.auto_field()
    lon = ma.auto_field()
    lat = ma.auto_field()
    temp = ma.auto_field()
    temp_min = ma.auto_field()
    temp_max = ma.auto_field()
    pressure = ma.auto_field()
    humidity = ma.auto_field()
    weather = ma.auto_field()
    l2_name = ma.auto_field()


geolocation_schema = GeolocationSchema()
geolocations_schema = GeolocationSchema(many=True)
