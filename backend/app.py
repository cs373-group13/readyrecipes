import json
from flask import Flask, request, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS
from init import app, db, ma
import models

Recipe = models.Recipe
recipe_schema = models.recipe_schema
recipes_schema = models.recipes_schema
Geolocation = models.Geolocation
geolocation_schema = models.geolocation_schema
geolocations_schema = models.geolocations_schema
Crops = models.GardenCrop
gardencrop_schema = models.gardencrop_schema
gardencrops_schema = models.gardencrops_schema

recipe_gardencrop = models.assoc_recipe_gardencrop
geo_garden = models.AssocGeoGarden
db.create_all()

PAGE_SIZE = 20
num_recipes = Recipe.query.count()
num_geolocations = Geolocation.query.count()
num_garden_crops = Crops.query.count()

# NOTE: This route is needed for the default EB health check route
@app.route("/")
def home():
    return "ok"


@app.route("/api/test")
def test():
    return "hello, world!"


@app.route("/api/recipes", methods=["GET"])
def get_recipes():
    query = Recipe.query.all()
    response = {"records": num_recipes, "data": recipes_schema.dump(query)}
    return response


@app.route("/api/recipes/<int:id_num>", methods=["GET"])
def get_recipe(id_num):
    query = Recipe.query.get(id_num)
    return recipe_schema.dump(query)


@app.route("/api/geolocation", methods=["GET"])
def get_geolocations():
    query = Geolocation.query
    response = {"records": num_recipes, "data": geolocations_schema.dump(query.all())}
    return response


@app.route("/api/geolocation/<int:id_num>", methods=["GET"])
def get_geolocation(id_num):
    query = Geolocation.query.get(id_num)
    return geolocation_schema.dump(query)


@app.route("/api/gardencrops", methods=["GET"])
def get_garden_crops():
    query = Crops.query.all()
    response = {"records": num_recipes, "data": gardencrops_schema.dump(query)}
    return response


@app.route("/api/gardencrops/<int:id_num>", methods=["GET"])
def get_garden_crop(id_num):
    query = Crops.query.get(id_num)
    return gardencrop_schema.dump(query)


@app.route("/api/vegetable_to_cities/<int:id_num>", methods=["GET"])
def vegetable_to_cities(id_num):
    query = geo_garden.query.filter(geo_garden.gardencropid == id_num).limit(20).all()
    ret = []
    for assoc in query:
        geolocations = Geolocation.query.filter(
            Geolocation.l2_name == assoc.l2_zone
        ).all()
        for location in geolocations:
            if len(ret) == 20:
                break
            ret.append(get_geolocation(location.id))
    return jsonify(ret)


@app.route("/api/vegetable_to_recipes/<int:id_num>", methods=["GET"])
def vegetable_to_recipes(id_num):
    query = (
        db.session.query(recipe_gardencrop)
        .filter(recipe_gardencrop.c.gardencrop_id == id_num)
        .limit(20)
        .all()
    )
    ret = []
    for assoc in query:
        ret.append(get_recipe(assoc.recipe_id))
    return jsonify(ret)


@app.route('/api/city_to_vegetables/<int:id_num>', methods=["GET"])
def city_to_vegetables(id_num):
    l2_name = Geolocation.query.get(id_num).l2_name
    query = geo_garden.query.filter(geo_garden.l2_zone==l2_name).all()
    crops = Crops.query
    ret = []
    for assoc in query:
        if (len(ret) == 20):
            break
        if (crops.get(assoc.gardencropid) != None):
            ret.append(get_garden_crop(assoc.gardencropid)) 
    return jsonify(ret)

@app.route('/api/city_to_recipes/<int:id_num>', methods=["GET"])
def city_to_recipes(id_num):
    l2_name = Geolocation.query.get(id_num).l2_name
    query = geo_garden.query.filter(geo_garden.l2_zone==l2_name).all()
    ret = []
    for assoc in query:
        crop_id = assoc.gardencropid
        query2 = db.session.query(recipe_gardencrop).filter(recipe_gardencrop.c.gardencrop_id==crop_id).all()
        for assoc2 in query2:
            if (len(ret) == 20):
                break
            ret.append(get_recipe(assoc2.recipe_id))
        else:
            continue
        break
    return jsonify(ret)

@app.route("/api/recipe_to_cities/<int:id_num>", methods=["GET"])
def recipe_to_cities(id_num):
    query = (
        db.session.query(recipe_gardencrop)
        .filter(recipe_gardencrop.c.recipe_id == id_num)
        .all()
    )
    ret = []
    for assoc in query:
        crop_id = assoc.gardencrop_id
        query2 = geo_garden.query.filter(geo_garden.gardencropid == crop_id).all()
        for assoc2 in query2:
            geolocations = Geolocation.query.filter(
                Geolocation.l2_name == assoc2.l2_zone
            ).all()
            for location in geolocations:
                if len(ret) == 20:
                    break
                ret.append(get_geolocation(location.id))
            else:
                continue
            break
        else:
            continue
        break
    return jsonify(ret)


@app.route("/api/recipe_to_vegetables/<int:id_num>", methods=["GET"])
def recipe_to_vegetables(id_num):
    query = (
        db.session.query(recipe_gardencrop)
        .filter(recipe_gardencrop.c.recipe_id == id_num)
        .limit(20)
        .all()
    ) #this only gives the id for the connected recipe and garden crop
    ret = []
    crops = Crops.query
    for assoc in query:
        ret.append(gardencrop_schema.dump(crops.get(assoc.gardencrop_id)))
    return jsonify(ret)


if __name__ == "__main__":
    app.run(debug=True, port=8080)
