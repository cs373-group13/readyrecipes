echo "Deploying Backend..."
cd backend
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 607778362084.dkr.ecr.us-east-2.amazonaws.com
docker build -t readyrecipes-backend .
docker tag readyrecipes-backend:latest 607778362084.dkr.ecr.us-east-2.amazonaws.com/readyrecipes-backend:latest
docker push 607778362084.dkr.ecr.us-east-2.amazonaws.com/readyrecipes-backend:latest
cd aws_deploy
eb deploy