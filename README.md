# ReadyRecipes

## Names, EIDs, and Gitlab IDs of team members

|  Name | UTEID  | GitLab ID  |
|---|---|---|
|Remus Wong|rcw2448|remusw|
|Timothy Situ|tws895|timothysitu|
|Daniel Young|day397|danyoungday|
|Ian Trowbridge|it3434|trowk|
|Tejna Dasari|td23366|tejnadas|

## Git SHA:
Phase 1: 4179d3f0bd1754b32dd2a6c02a28897d17fa87d5

Phase 2: 7c4d7e07a6478834b77792d7a1efe54e9671e0ee

Phase 3: 3d8ece61fe27d5899ab54b059de231f09595bd7e

Phase 4: ec358e90312788603552f38e6acf0e5544f5a9a9

## Project Leader:
Phase 1: Remus Wong

Phase 2: Daniel Young

Phase 3: Ian Trowbridge

Phase 4: Timoty Situ

## Gitlab Pipelines:
https://gitlab.com/cs373-group13/readyrecipes/-/pipelines

## WEBSITE LINK:
https://readyrecipes.me/

## Presentation LINK:
https://www.youtube.com/watch?v=WLZxFt9YdDg

## Postman Link:
https://documenter.getpostman.com/view/14748973/TzJrDKyn

## Phase 1 Completion Time:

| Name | Estimated | Actual |
|------|-----------|--------|
|Remus Wong|10|10|
|Timothy Situ|10|8|
|Daniel Young|5|8|
|Ian Trowbridge|12|8|
|Tejna Dasari|10|10|

## Phase 2 Completion Time:

| Name | Estimated | Actual |
|------|-----------|--------|
|Remus Wong|15|20|
|Timothy Situ|15|15|
|Daniel Young|15|20|
|Ian Trowbridge|10|18|
|Tejna Dasari|10|15|

## Phase 3 Completion Time:

| Name | Estimated | Actual |
|------|-----------|--------|
|Remus Wong|15|10|
|Timothy Situ|10|7|
|Daniel Young|10|8|
|Ian Trowbridge|12|15|
|Tejna Dasari|10|15|

## Phase 4 Completion Time:

| Name | Estimated | Actual |
|------|-----------|--------|
|Remus Wong|8|8|
|Timothy Situ|6|9|
|Daniel Young|10|8|
|Ian Trowbridge|9|8|
|Tejna Dasari|6|6|

## Comments:

Daniel has a ton of commits and there's a ton of pipelines because he was 
playing with gitlab CI and trying to set up Continuous Deployment
(before he realized it was too complicated and just got rid of it)
