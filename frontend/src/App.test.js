import React from 'react';
import '@testing-library/jest-dom/extend-expect'
import * as Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { mount, shallow } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';

import Home from "./components/Home/Home"
import Navbar from "./components/Navbar/Navbar"
import About from "./components/About/About"
import SearchBar from "./components/Search/SearchBar"
import Search from "./components/Search/Search"
import GardenCrops from "./components/garden crops/GardenCrops"
import Geolocation from "./components/geolocation/geolocation"
import RecipeModel from "./components/recipes/recipeModel"
import CropsTable from "./components/garden crops/cropsTable"
import GeoTable from "./components/geolocation/geolocationTable"
import RecipeTable from "./components/recipes/recipeTable"

import recipeItem from "./components/recipes/DummyData.json"
import geolocationItem from "./components/geolocation/DummyData.json"
import cropItem from "./components/garden crops/DummyData.json"



Enzyme.configure({ adapter: new Adapter() });

/* Simple Unit Tests Home Page */
describe("Home Page", () => {
  const wrapper = shallow(<Home />);
  it("is not Undefined", () => {
      expect(wrapper).not.toBeUndefined();
    });
    
  it("has Correct Header", () => {
    expect(wrapper.find('h1')).toHaveLength(1)
  });
    
  it("has Seach Bar", () => {
    expect(wrapper.find('SearchBar')).toHaveLength(1)
  });
});

describe("Navbar", () => {
  const wrapper = shallow(<Navbar />);
  it("is not Undefined", () => {
    expect(wrapper).not.toBeUndefined();
  });
  
  it("has Correct Number of Links", () => {
    expect(wrapper.find('Link')).toHaveLength(8)
  });
});

describe("About Page", () => {
  const wrapper = shallow(<About />);

  it("is not Undefined", () => {
    expect(wrapper).not.toBeUndefined();
  });
  
  it("has Correct Number of Cards", () => {
    expect(wrapper.find('Card')).toHaveLength(5);
  });

});

describe("Search Bar", () => {
  const wrapper = shallow(<SearchBar query="Test"/>);
  it("is not Undefined", () => {
    expect(wrapper).not.toBeUndefined();
  });

  it("has a Submit Button", () => {
    expect(wrapper.find('Button')).toHaveLength(1);
  });

  it("has a Correct State", () => {
    expect(wrapper.state().query).toEqual("Test");
  });
});

describe("Sitewide Search", () => {
  const wrapper = shallow(<Search />);
  it("is not Undefined", () => {
    expect(wrapper).not.toBeUndefined();
  });

  it("has a Search Bar", () => {
    expect(wrapper.find('SearchBar')).toHaveLength(1);
  });

  it("has Garden Crops Results", () => {
    expect(wrapper.find('GardenCrops')).toHaveLength(1);
  });

  it("has Recipe Results", () => {
    expect(wrapper.find('RecipeModel')).toHaveLength(1);
  });
});

describe("GardenCrops", () => {
  const wrapper1 = shallow(<GardenCrops/>);
  const wrapper2 = shallow(<CropsTable data={cropItem} searchText=""/>);
  it("is not Undefined", () => {
    expect(wrapper1).not.toBeUndefined();
  });

  it("is loading", async () => {
    expect(wrapper1.find('Loading')).toHaveLength(1)
  });

  it("table is not undefined", () => {
    expect(wrapper2).not.toBeUndefined();
  });
});

describe("Geolocations", () => {
  const wrapper = shallow(<Geolocation/>);
  const wrapper2 = shallow(<GeoTable data={geolocationItem} searchText=""/>);

  it("is not Undefined", () => {
    expect(wrapper).not.toBeUndefined();
  });

  it("is loading", () => {
    expect(wrapper.find('Loading')).toHaveLength(1)
  });

  it("table is not undefined", () => {
    expect(wrapper2).not.toBeUndefined();
  });
});

describe("Recipes", () => {
  
  const wrapper = shallow(<RecipeModel/>);
  const wrapper2 = shallow(<RecipeTable data={recipeItem} searchText=""/>);

  it("is not Undefined", () => {
    expect(wrapper).not.toBeUndefined();
  });

  it("is loading", async () => {
    expect(wrapper.find('Loading')).toHaveLength(1)
  });

  it("table is not undefined", () => {
    expect(wrapper2).not.toBeUndefined();
  });
});