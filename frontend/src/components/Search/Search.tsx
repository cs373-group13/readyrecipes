import React from "react"
import RecipeModel from "../recipes/recipeModel";
import GardenCrops from "../garden crops/GardenCrops"
import Geolocation from "../geolocation/geolocation";
import SearchBar from "./SearchBar"

interface IProp {}

interface IState {
  query: string;
}

class Search extends React.Component<IProp, IState> {
    constructor(props: IProp) {
      super(props);
      const url = window.location.href;
      const split_url = url.split("/");
      const query = split_url[split_url.length - 1] === "search" ? "" : split_url[split_url.length - 1];
      console.log("SEARCH", query);

      this.state = {query: query};
    }

    render() {
        return (
          <div>
              <SearchBar query={this.state.query}/>
              <div>
                <Geolocation initSearch={this.state.query}/>
                <RecipeModel initSearch={this.state.query}/>
                <GardenCrops initSearch={this.state.query}/>
              </div>
            </div>
        );
    }
}

export default Search;