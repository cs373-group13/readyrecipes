import React from "react"
import { Button } from 'react-bootstrap'
import TextField from '@material-ui/core/TextField';


interface IProp {
  query: string;
}

interface IState {
  query: string;
}

class SearchBar extends React.Component<IProp, IState> {
    constructor(props: IProp) {
      super(props);
      const query = this.props.query;
      console.log("SEARCH", query);

      this.state = {query: query};
      this.updateFormEntry = this.updateFormEntry.bind(this);
      this.performQuery = this.performQuery.bind(this);
      this.keyPress = this.keyPress.bind(this);
    }
    updateFormEntry = (event) => {
      const searchText: string = event.target.value;
      console.log("Search: ", searchText);
      this.setState({
        query: searchText
      });

    }

    keyPress(e){
      if(e.keyCode === 13){
         console.log('value', e.target.value);
         this.performQuery(e);
      }
   }

    performQuery = (event) => {
      console.log("Query");
      window.location.assign("/search/" + this.state.query);
    }

    render() {
        return (
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <TextField 
                    id="standard-basic"
                    label="Sitewide Search"
                    onChange={this.updateFormEntry}
                    value={this.state.query}
                    onKeyDown={this.keyPress}
                />
                <Button onClick={this.performQuery}>
                    Search
                </Button>{' '}
            </div>
        );
    }
}

export default SearchBar;