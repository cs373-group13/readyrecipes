export const sortIncreasing = (data, field) => {
    return data.sort((item1, item2) => {
        return item1[field] - item2[field];
    });
}

export const sortDecreasing = (data, field) => {
    return data.sort((item1, item2) => {
        return item2[field] - item1[field];
    });
}