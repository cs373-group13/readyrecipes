import { Card, ListGroup } from 'react-bootstrap';
import { convertToObject } from 'typescript';


const ITEMS_PER_ROW:number = 5;

function RenderRow(data: any[], Component: any) {
    console.log("Rendering Row");
    console.log(data)
    return (
        <ListGroup.Item>
            {data.map((key, idx) => (
                <Component data={key} index={idx}></Component>
            ))}
        </ListGroup.Item>
    );
}

function Grid({data, Component}: any) {
    const rows:any[] = [];
    for (let i:number = 0; i < data.length; i += ITEMS_PER_ROW) {
        rows.push(data.slice(i, Math.min(i + ITEMS_PER_ROW, data.length)));
    }
    const allrows = rows.map((key, idx) => RenderRow(key, Component))

    console.log("Creating Grid")
    return (
        <Card>
            <ListGroup horizontal>
                {allrows}
            </ListGroup>
        </Card>

    );
}

export default Grid;