import { Link } from "react-router-dom";
import { Grid, Typography, Container } from '@material-ui/core';

 
export enum ModelType {
    Geolocation,
    Recipe,
    Crop
}

function renderGeolocationRelation(data: any, index: any) {
    return (
        <Grid item xs={2} key={index} >
            <Link to={`/geolocationInstance/${data['id']}`} style={{ textDecoration: 'none' }}>
                    <Typography display="inline">
                        {data["name"]}
                    </Typography>
             </Link>
        </Grid>
    );
}

function renderRecipeRelation(data: any, index: any) {
    console.log("Rendering Recipe");
    return (
    <Grid item xs={2} key={index} >
        <Link to={`/recipeInstance/${data['id']}`} style={{ textDecoration: 'none' }}>
        <Typography display="inline">
            {data["title"]}
        </Typography>
        </Link>
    </Grid>
    );
}

function renderCropRelation(data: any, index: any) {
    console.log("Rendering Crops");
    return (
        <Grid item xs={2} key={index} >
            <Link to={`/gardenInstance/${data['id']}`} style={{ textDecoration: 'none' }}>
            <Typography display="inline">
                {data['common_name']}
            </Typography>  
            </Link>
        </Grid>
    );
}

function RelatedSection(related: relatedObjI) {
    function renderCard(realtedObj: relatedObjI) {
        console.log("Rendering Section");
        switch(related.model_type) {
            case(ModelType.Geolocation): {
                return (
                    related.data.map((key, index) => {
                        return renderGeolocationRelation(key, index);
                    })
                );
            }

            case(ModelType.Recipe): {
                console.log("Calling Grid for Recipe");
                return (
                    related.data.map((key, index) => {
                        return renderRecipeRelation(key, index);
                    })
                );
            }

            case(ModelType.Crop):
            default: {
                console.log("Calling Grid for Crop");
                return (
                    related.data.map((key, index) => {
                        return renderCropRelation(key, index);
                    })
                );
            }
        }
    }
    return (
        <Container>
            <h3>{related.header}</h3>
            <Grid container alignItems="stretch">
                {renderCard(related)}
            </Grid>
        </Container>
    );
}

export function AllRelatedSections({related1, related2}: any) {
    return (
        <Container>
            {RelatedSection(related1)}
            {RelatedSection(related2)}
        </Container>
    );
}

export interface relatedObjI {
    data: any;
    header: string;
    model_type: ModelType;
}