import React from 'react';
import GoogleMapReact from 'google-map-react';
import { Container as BootContainer } from 'react-bootstrap';

type LocationKeys = {
  address?: string,
  lat: number,
  lng: number
}

type MapSectionProps = {
  location?: LocationKeys,
  zoomLevel?: number,
  mapTypeId?: string
}

function MapSection(params: MapSectionProps) {
  const { location = { lat: 30.27, lng: -97.74 },
    zoomLevel = 10, mapTypeId = "hybrid" } = params;
    console.log(process.env.REACT_APP_MAPS_API_KEY)
  return (
    <BootContainer fluid>
      <div className="map" style={{justifyContent:'center' }}>
        <div className="google-map" style={{width: "300px", height: "300px", justifyContent:'center'}}>
          <GoogleMapReact
            bootstrapURLKeys={{ key: process.env.REACT_APP_MAPS_API_KEY }}
            defaultCenter={location}
            defaultZoom={zoomLevel}
            options={function () { return { mapTypeId: mapTypeId } }}
          />
        </div>
      </div>
    </BootContainer>
  );
}

export default MapSection;
