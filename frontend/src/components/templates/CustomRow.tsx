import Highlight from "./Highlight";
import { TableCell, TableRow, Typography } from "@material-ui/core";


const CustomRow = (props) => {
    const searchText:string|null = props.searchText;
    const data: any[] = props.data;
    const showColumns:boolean[] = props.showColumns;
    return (
        <TableRow onClick = {() => {return props.onRowClick(data[0])}} style={{cursor: "pointer"}}>
            {data.map((datum, idx) => {
                const word = datum === null ? null : datum.toString();
                return (
                showColumns[idx] ?
                <TableCell key={idx}>
                    <Typography>
                        <Highlight word={word} searchText={searchText}></Highlight>
                    </Typography>
                </TableCell> :
                null
                );
            })}
        </TableRow>
        );

}

export default CustomRow;