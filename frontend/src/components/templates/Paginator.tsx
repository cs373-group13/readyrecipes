// Credit to digitalocean for their tutorial on pagination. The tutorial can be
// found at https://www.digitalocean.com/community/tutorials/how-to-build-custom-pagination-with-react
import React, { Component } from 'react';
import Pagination from '@material-ui/lab/Pagination';


interface PaginationProps {
    currPage: number;
    totalRecord: number,
    totalPages: number,
    pageLimit: number,
    pageNeighbours: number,
    onPageChanged: (pageInfo: PaginationDataI) => any
};
  
interface PaginationState {
      currentPage: number
};

export interface PaginationDataI {
    currentPage: number,
    totalPages: number,
    pageLimit: number,
    totalRecords: number
}

export class Paginatior extends Component<PaginationProps, PaginationState> {
    pageLimit:number;           // Number of records to show per page
    totalRecords:number;        // total number of records to be paginated
    pageNeighbours:number;      // Number of page numbers to show on each side
                                // of current page
    totalPages: number;         // Total number of pages for model
    constructor(props: PaginationProps) {
        super(props);
        console.log("Paginator Constructor")
        this.pageLimit = props.pageLimit;
        this.totalRecords = props.totalRecord === undefined ? 0 : props.totalRecord;
        this.pageNeighbours = props.pageNeighbours;

        this.totalPages = this.props.totalPages;
        this.state = { currentPage: props.currPage };
    }

    render() {
      const currentPage = this.props.currPage;
      const totalPages:number = this.props.totalPages;
      this.totalPages = totalPages;
      if (!this.totalRecords || this.totalPages === 1) return null;

      return (
        <Pagination count={totalPages} page={currentPage} onChange={this.handleClick} />
      );
    }
    
      gotoPage = page => {
        console.log("GOTO PAGE")
        console.log(page)
        const { onPageChanged = f => f } = this.props;
        const currentPage:number = Math.max(1, Math.min(page, this.totalPages));
        const paginationData: PaginationDataI = {
          currentPage,
          totalPages: this.totalPages,
          pageLimit: this.pageLimit,
          totalRecords: this.totalRecords
        };
    
        this.setState({ currentPage }, () => onPageChanged(paginationData));
      }
    
      handleClick = (evt, value) => {
        this.gotoPage(value);
      }
}
