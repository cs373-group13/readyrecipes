import Highlighter from "react-highlight-words";

const Highlight = (props) => {
    const searchText:string|null = props.searchText;
    const search = searchText === null ? [""] : searchText.trim().split(' ');
    const word = props.word === null ? ""  : props.word;
    return <Highlighter
            highlightClassName="highlighter"
            searchWords={search}
            autoEscape={true}
            textToHighlight={word}
        />

}

export default Highlight;