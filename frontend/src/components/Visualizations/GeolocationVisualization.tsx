import useAxios from "axios-hooks";
import Gradient from "javascript-color-gradient";
import React, { useEffect, useState } from 'react';
import { Container as ContainerBoot, Row } from 'react-bootstrap';
import { ScatterChart, Scatter, Cell, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { Container, Typography } from '@material-ui/core';
import Loading from '../Loading/Loading'

export default function GeolocationPlot() {
    const [{ data, loading, error }] = useAxios({
        url: `${process.env.REACT_APP_API_URL}/geolocation`
      });
    const [width] = useState(1000);
    const [height] = useState(1000);
    const [usedData, setData] = useState<any[]>([]);
    const [colors, setColors] = useState<string[]>([]);

    const getData = (lon: number, lat: number) => {
        for (let i = 0; i < usedData.length; i++) {
            if (usedData[i].lat === lat && usedData[i].lon === lon) {
                return usedData[i];
            }
        }
        return null;
    }

    const CustomTooltip = ({ active, payload, label }:any) => {
        if (active && payload && payload.length) {
            const d = getData(payload[0].value, payload[1].value);
          return (
            <Container className="custom-tooltip" style={{
              backgroundColor: 'white'
            }}>
              {d === null ? null : 
                <div>
                <Typography className="label">{d.name}</Typography>
                <Typography className="label">{`${d.temp} Degrees Celcius`}</Typography>
                </div>
              }
              <Typography className="label">{`${payload[0].name} : ${payload[0].value}${payload[0].unit}`}</Typography>
              <Typography className="label">{`${payload[1].name} : ${payload[1].value}${payload[1].unit}`}</Typography>
            </Container>
          );
        } else {
            return null;
        }
    }

    useEffect(() => {
        if (data && data['data'].length > 0) {
            const usedData = data['data'].filter((d, idx) => {
              return idx % 40 === 0;
            }).map(k => {
              return {lat: k.lat, lon: k.lon, temp: Math.round(k.temp - 273.15), name: k.name}
            });
            
            const extremeTemps = usedData.reduce((d, e) => {
                d.max = e.temp > d.max ? e.temp : d.max;
                d.min = e.temp < d.min ? e.temp : d.min;
                return d
            }, {"max": usedData[0].temp, "min": usedData[0].temp});
            const range = extremeTemps.max - extremeTemps.min;

            const NUM_COLORS = 100;
            const color1 = "#33CCFF";
            const color2 = "#FF9900";
            const colorGradient = new Gradient();
            colorGradient.setMidpoint(NUM_COLORS);
            colorGradient.setGradient(color1, color2);
            const colorKey = colorGradient.getArray();
            const colors = usedData.map(k => {
                let mappedTemp = Math.round((k.temp - extremeTemps.min) / range * NUM_COLORS);
                mappedTemp = mappedTemp >= NUM_COLORS ? NUM_COLORS - 1 : mappedTemp;
                mappedTemp = mappedTemp < 0 ? 0 : mappedTemp;
                return colorKey[mappedTemp];
            });
            setColors(colors);
            setData(usedData);
        }

    }, [data, height, width]);
    
    if (loading) return(
      <ContainerBoot fluid>
          <Row className='justify-content-center'>
              <Loading />
          </Row>
      </ContainerBoot>
    );
    if (error) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography>Error in fetching API!</Typography>
            </Row>
        </ContainerBoot>
        
    );

    return (
        <ContainerBoot fluid>
          <Row className='justify-content-center'>
            <Typography component="h2" variant="h3" align="center" color="textPrimary">
                    Temperature for Cities vs Longitude and Latitude
            </Typography>
          </Row>
          <Row className='justify-content-center'>
            <ResponsiveContainer width={width} height={height}>
              <ScatterChart
                width={height}
                height={width}
                margin={{
                  top: 20,
                  right: 20,
                  bottom: 20,
                  left: 20,
                }}
              >
                <CartesianGrid />
                <XAxis type="number" dataKey="lon" name="Longitude" unit="&#176;"  />
                <YAxis type="number" dataKey="lat" name="Latitude" unit="&#176;" />
                <Tooltip cursor={{ strokeDasharray: '3 3' }} content={<CustomTooltip />} />
                <Scatter name="Temperature vs Longitude and Latitude" data={usedData} fill="#8884d8">
                  {usedData.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
                  ))}
                </Scatter>
              </ScatterChart>
            </ResponsiveContainer>
          </Row>
      </ContainerBoot>
    );
}
