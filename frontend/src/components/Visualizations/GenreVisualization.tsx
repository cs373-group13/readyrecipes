import React, { useEffect, useState } from 'react';
import { Container as ContainerBoot, Row } from 'react-bootstrap'
import { PieChart, Pie, Cell, ResponsiveContainer, Tooltip } from 'recharts';
import { Container, Typography } from '@material-ui/core';
import useAxios from "axios-hooks";
import Loading from '../Loading/Loading'


function GenreVisualization() {

    const [{ data, loading, error }] = useAxios({
        url: 'https://booksforyou.me/api/authors'
    });

    const COLORS = ['#538505', '#bda308', '#84b20b', '#335f04', '#e6d57e', '#443e04', '#887804', '#c3d233', '#224704', '#645a04'];

    const [formattedData, setData] = useState<any[]>([]);

    //Get formatted data when the API call finishes.
    useEffect(() => {
        if(data && data['authors'].length > 0) {
            var dict = {}
            var formattedData = [] as any;
            //Construct dictionary with genres and counts
            for(let i = 0; i < data["authors"].length; i++) {
                var author = data["authors"][i];
                if(author["genres"] && author['genres'].length > 0) {
                    for(let j = 0; j < author['genres'].length; j++) {
                        var genre = author['genres'][j];
                        //If the genre is already there, increment the count by 1
                        if(dict[genre]) {
                            dict[genre]++;
                        }
                        //Otherwise add it to the dict with count 1
                        else {
                            dict[genre] = 1
                        }
                    }
                }
            }
            
            //Delete all genres with freq < 10
            for (const [genre, count] of Object.entries(dict)) {
                if(Number(count) < 10) {
                    delete dict[genre];
                }
            }

            //Transfer dictionary to array
            for (const [genre, count] of Object.entries(dict)) {
                formattedData.push({name: genre, value: count});
            }

            //Transfer temp data to formatted data
            setData(formattedData)
        }
    }, [data]);

    const CustomTooltip = ({ active, payload, label } : any) => {
        if (active) {
            return (
                <Container className="custom-tooltip" style={{
                    backgroundColor: 'white'
                }}>
                    <Typography className='label'>{`${payload[0].name} : ${payload[0].value} Authors`}</Typography>
                </Container>
            );
        }

        return null;
    };

    if (loading) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Loading />
            </Row>
        </ContainerBoot>
        );
    if (error) {
        console.log(data);
        return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography>Error in fetching API!</Typography>
            </Row>
        </ContainerBoot>
        
        );
    }

    return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                    How Many Authors Write Each Common Genre
                </Typography>
            </Row>
            <Row className='justify-content-center'>
                <ResponsiveContainer width='90%' height={500}>
                    <PieChart width={400} height={500}>
                        <Pie
                            data={formattedData}
                            cx='50%'
                            cy='50%'
                            outerRadius={200}
                            fill={'#8884d8'}
                            dataKey='value' 
                        >
                            {formattedData.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={COLORS[(index) % COLORS.length]} />
                            ))}
                        </Pie>
                        <Tooltip content={<CustomTooltip />} />
                    </PieChart>
                </ResponsiveContainer>
            </Row>
        </ContainerBoot>
    );

}

export default GenreVisualization;