import { Container } from '@material-ui/core';

import GeolocationPlot from "./GeolocationVisualization"
import CropPlot from "./CropsVisualization"
import RecipePlot from "./RecipeVisualizations"


export default function Visualization() {
    return (
        <Container maxWidth="xl">
            <GeolocationPlot />
            <CropPlot />
            <RecipePlot />
        </Container>
    );
}
