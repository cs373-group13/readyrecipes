import { Container } from '@material-ui/core';
import RatingVisualization from './RatingVisualization'
import GenreVisualization from './GenreVisualization'
import QuoteVisualization from './QuoteVisualization'

export default function ProviderVisualizations() {
    return (
        <Container maxWidth="xl">
            <RatingVisualization />
            <GenreVisualization />
            <QuoteVisualization />
        </Container>
    );
}