import useAxios from "axios-hooks";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import React, { useEffect, useState } from 'react';
import { Container as ContainerBoot, Row } from 'react-bootstrap';
import { Typography } from '@material-ui/core';
import Loading from '../Loading/Loading'
const RANGE = 20;

export default function RecipePlot() {
    const [width] = useState(1000);
    const [height] = useState(1000);
    const [usedData, setData] = useState<any[]>([]);

    const [{ data, loading, error }] = useAxios({
        url: `${process.env.REACT_APP_API_URL}/recipes`
      });
    useEffect(() => {
        if (data && data['data'].length > 0) {
            const groupedData = {};
            for (let i = 0; i < data['data'].length; i++) {
                const k = data['data'][i];
                const lower_bound = k['pricePerServing'] - k['pricePerServing'] % RANGE;
                const upper_bound = lower_bound + RANGE;
                const bin = lower_bound.toString() + "-" + upper_bound.toString();
                if (!groupedData.hasOwnProperty(bin)) {
                    groupedData[bin] = 0
                }
                groupedData[bin] += 1;
            }
            let usedData = Object.keys(groupedData).map(k => {
                return {"name": k, "value": groupedData[k]}
            });
            usedData = usedData.sort((a, b) => {
                const anum = parseInt(a['name'].split('-')[0]);
                const bnum = parseInt(b['name'].split('-')[0]);
                return anum - bnum;
            })
            setData(usedData);
        }   
    }, [data, height, width]);
    
    if (loading) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Loading />
            </Row>
        </ContainerBoot>
    );
    if (error) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography>Error in fetching API!</Typography>
            </Row>
        </ContainerBoot>
        
    );
    return (
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                    Number of Recipes vs Price Per Serving
                </Typography>
            </Row>
            <Row className='justify-content-center'>
                <BarChart width={width} height={height} data={usedData}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis interval={0} height={125} angle={80} textAnchor="start"
                            dataKey="name" name="Price Per Serving" />
                    <YAxis />
                    <Tooltip formatter={(value, name, props) =>
                                        [value, "Number of Recipes"] } />
                    <Bar dataKey="value" fill="#33ccff">
                        {usedData.map((entry, index) =>
                                <Cell key={"barKey" + index} fill={"#33ccff"}/>)}
                    </Bar>
                </BarChart>
            </Row>
        </ContainerBoot>
    );

}