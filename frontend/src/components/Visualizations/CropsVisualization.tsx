import useAxios from "axios-hooks";
import {Cell, PieChart, Pie, Tooltip} from "recharts";
import Gradient from "javascript-color-gradient";
import React, { useEffect, useState } from 'react';
import { Container as ContainerBoot, Row } from 'react-bootstrap';
import { Typography } from '@material-ui/core';
import Legend from "recharts/lib/component/Legend";
import Loading from '../Loading/Loading'  


function groupBy(arrayObjects, key) {
    console.log(arrayObjects)
    return arrayObjects.reduce(function(result, currentObject) {
        const val = currentObject[key]
        result[val] = result[val] || []
        result[val].push(currentObject)
        return result
    }, {})
}

export default function CropPlot() {
    const [width] = useState(1000);
    const [height] = useState(1000);
    const [usedData, setData] = useState<any[]>([]);
    const [colors, setColors] = useState<string[]>([]);
    const [{ data, loading, error }] = useAxios({
        url: `${process.env.REACT_APP_API_URL}/gardencrops`
    });
    useEffect(() => {
        if (data && data['data'].length > 0) {
            const groupedData = groupBy(data.data, 'family');
            const usedData = Object.keys(groupedData).map(k => {
                return {"name": k, "value": groupedData[k].length}
            });
            const color1 = "#990000";
            const color2 = "#006600";
            const color3 = "#CC33FF";
            const colorGradient = new Gradient();
            colorGradient.setMidpoint(20);
            colorGradient.setGradient(color1, color2);
            const colors = colorGradient.getArray();

            colorGradient.setMidpoint(usedData.length - 20);
            colorGradient.setGradient(color2, color3);
            colors.push(...colorGradient.getArray());
            
            setColors(colors);
            setData(usedData.sort((a, b) => {
                return b['value']-a['value'];
            }));
        }

        }, [data, height, width]);
    
    if (loading) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Loading />
            </Row>
        </ContainerBoot>
    );
    if (error) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography>Error in fetching API!</Typography>
            </Row>
        </ContainerBoot>
        
    );
    return (
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                    Number of Plants for Each Family
                </Typography>
            </Row>
            <Row className='justify-content-center'>
                <PieChart width={width} height={height}>
                    <Pie dataKey="value" data={usedData} cx={width/2} cy={height/2}
                            outerRadius={width/4} fill={'#c89664'} >
                        {usedData.map((entry, index) =>
                                    <Cell key={"pieKey" + index}
                                    fill={colors[index % colors.length]}/>)}
                    </Pie>
                    <Tooltip />
                    <Legend iconSize={50}/>
                </PieChart>
            </Row>
        </ContainerBoot>
    );
}


