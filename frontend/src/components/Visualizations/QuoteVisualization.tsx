import React, { useEffect, useState } from 'react';
import { Container as ContainerBoot, Row } from 'react-bootstrap';
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import { Container, Typography } from '@material-ui/core';
import useAxios from "axios-hooks";
import Loading from '../Loading/Loading'

export default function QuoteVisualization() {

    const [{ data, loading, error }] = useAxios({
        url: 'https://booksforyou.me/api/quotes'
    });

    const [formattedData, setData] = useState<any[]>([]);

    useEffect(() => {
        if(data && data['quotes'].length > 0) {
            var formattedData = [] as any;
            for(let i = 0; i < data['quotes'].length; i++) {
                var quote = data['quotes'][i];
                if(quote['num_unique_words'] && quote['score'] && quote['author_name']) {
                    formattedData.push({x : quote['num_unique_words'], y : quote['score'], name : quote['author_name']})
                }
            }
            setData(formattedData);
        }
    }, [data]);

    //Create custom tooltip that shows book name along with data
    const CustomTooltip = ({ active, payload, label } : any) => {
        if (active) {
            return (
                <Container className="custom-tooltip" style={{
                    backgroundColor: 'white'
                }}>
                    <Typography className='label'>Author: {payload[0]['payload']['name']}</Typography>
                    <Typography className='label'>{payload[0]['name']} : {payload[0]['value']}</Typography>
                    <Typography className='label'>{payload[1]['name']} : {payload[1]['value']}</Typography>
                </Container>
            );
        }
    
        return null;
    };


    if (loading) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Loading />
            </Row>
        </ContainerBoot>
    );
    if (error) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography>Error in fetching API!</Typography>
            </Row>
        </ContainerBoot>
        
    );

    return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                    Number of Unique Words in Quote vs. Quote Score
                </Typography>
            </Row>
            <Row className='justify-content-center'>
                <ResponsiveContainer width="90%" height={500}>
                    <ScatterChart
                        width={400}
                        height={500}
                        margin={{
                            top: 20,
                            right: 20,
                            bottom: 20,
                            left: 20,
                        }}
                    >
                        <CartesianGrid />
                        <XAxis type="number" dataKey="x" name="Unique Words" />
                        <YAxis type="number" dataKey="y" name="Quote Score" domain={[-1.5, 1.5]}/>
                        <Tooltip content={<CustomTooltip />} />
                        <Scatter name="Author Rating vs. Number of Books Published" data={formattedData} fill='#72b01d' />
                    </ScatterChart>
                </ResponsiveContainer>
            </Row>
        </ContainerBoot>
    );

}