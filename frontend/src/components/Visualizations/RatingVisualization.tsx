import React, { useEffect, useState } from 'react';
import { Container as ContainerBoot, Row } from 'react-bootstrap';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { Typography } from '@material-ui/core';
import useAxios from "axios-hooks";
import Loading from '../Loading/Loading'

function RatingVisualization() {

    const [{ data, loading, error }] = useAxios({
        url: 'https://booksforyou.me/api/books'
    });

    const [formattedData, setData] = useState<any[]>([]);

    //Get formatted data when the API call finishes.
    useEffect(() => {
        if(data && data['books'].length > 0) {
            var formattedData = [] as any;
            //Keep track of how many books have a given rating
            var dict = {}
            for(let i = 0; i < data["books"].length; i++) {
                var book = data['books'][i];
                if(book['avg_rating']) {
                    if(dict[book['avg_rating']]) {
                        dict[book['avg_rating']] += 1;
                    }
                    else {
                        dict[book['avg_rating']] = 1;
                    }
                }
            }
            var sorted = [] as any;
            for(var key in dict) {
                sorted[sorted.length] = Number(key);
            }
            sorted.sort();
            for(let i = 0; i < sorted.length; i++) {
                formattedData.push({name: sorted[i], Count: dict[sorted[i]]});
            }
            setData(formattedData)
        }
    }, [data]);

    if (loading) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Loading />
            </Row>
        </ContainerBoot>
    );
    if (error) return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography>Error in fetching API!</Typography>
            </Row>
        </ContainerBoot>
        
    );

    return(
        <ContainerBoot fluid>
            <Row className='justify-content-center'>
                <Typography component="h2" variant="h3" align="center" color="textPrimary">
                    Distribution of Book Ratings
                </Typography>
            </Row>
            <Row className='justify-content-center'>
                <ResponsiveContainer width="90%" height={500}>
                    <BarChart
                    width={400}
                    height={500}
                    data={formattedData}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                    >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="Count" fill='#72b01d' />
                    </BarChart>
                </ResponsiveContainer>
            </Row>
        </ContainerBoot>
    );
}

export default RatingVisualization;