import React from 'react';
import './card-style.css';


const Card = props => {
    return(
        <div className="card text-center shadow">
            <div className="overflow">
                <img src={props.person.picture} alt = "person" className = "card-img-top"/>
            </div>
            <div className="card-body text-dark">
                <h4 className = 'card-title'> {props.person.name} </h4>
                <p className="card-text text-secondary">
                    <p> GitLab Name: {props.person.committer_name} </p>
                    <p> Email Address: {props.person.email} </p>
                    <p> Commit Count: {props.person.commits} </p>
                    <p> Issue Count: {props.person.issues} </p>
                    <p> Unit Tests Count: {props.person.unitTests} </p>
                    <p> Bio: {props.person.bio} </p>
                </p>
                {/* <a href="#" className = "btn btn-outline-success"> Go Anywhere </a> */}
            </div>
        </div>
    );
}

export default Card;
