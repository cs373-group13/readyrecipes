import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles({
    card: {
      maxWidth: 400,
      minWidth: 200,
    },
    media: {
      height: 200,
      minWidth: 200,
    },
    root: {
      flexGrow: 1,
    }
  });

  const details = [
    {
      name: "Spoonacular",
      source: "https://spoonacular.com/food-api",
      img_url: "https://imgur.com/zKcj250.png",
      description: "API for recipes",
    },
    {
      name: "Openweather",
      source: "https://openweathermap.org/api",
      img_url: "https://i.imgur.com/op7Z6gd.png",
      description: "API for geolocation data",
    },
    {
        name: "Treffle",
        source: "https://trefle.io",
        img_url: "https://imgur.com/v6c3hrb.png",
        description: "API for gardencrop information",
    }
  ];
  

export default function DetailsInfo() {
  const classes = useStyles();

  return (
    <Container>
      <Typography variant="h3" gutterBottom>
        API Used
      </Typography>
      <Grid container className={classes.root} spacing={4}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={4}>
            {details.map((detail: any) => (
              <Grid key={detail} item>
                <Card className={classes.card}>
                  <CardActionArea href={detail.source}>
                    <CardMedia
                      className={classes.media}
                      image={detail.img_url}
                      title={detail.name}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {detail.name}
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                        {detail.description}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}
