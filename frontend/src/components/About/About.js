import React, { Component } from 'react';
import Memberinfo from './Memberinfo';
import Card from '../Cards/cardUI'
import axios from 'axios';
import Tools from './Tools'
import Details from './Details'
import API from './apiUsed'
import "./About.css"

class About extends Component {
    state = {
        commitInfo: [],
        members: Memberinfo,
        totalCommits: Number,
        totalIssues: Number
    }

    componentDidMount() {
      axios.get(`https://gitlab.com/api/v4/projects/24674563/repository/contributors`)
        .then(res => {
          const commits = res.data
          this.setState({ ...this.state, commitInfo: commits})
          this.setMemberInfo();
      })

       this.setIssueInfo();
    }

    setMemberInfo() {
      let sum = 0;
      let mems = this.state.members;
      for(let k=0; k<this.state.commitInfo.length; k++) {
        for(let i=0; i<this.state.members.length; i++) {
          if(this.state.commitInfo[k].email === this.state.members[i].email) {
            mems[i].commits = this.state.commitInfo[k].commits;
            sum += this.state.commitInfo[k].commits;
          }
        }
      }
      this.setState({members: mems, totalCommits: sum})
    }

    setIssueInfo() {
      for(let i=0; i<this.state.members.length; i++) {
        axios.get(`https://gitlab.com/api/v4/projects/24674563/issues_statistics?author_username=` + this.state.members[i].committer_name)
        .then(res => {
          const issues = res.data.statistics.counts.all;
          // 1. Make a shallow copy of the items
          let members = [...this.state.members];
          // 2. Make a shallow copy of the item you want to mutate
          let member_issue = {...members[i]};
          // 3. Replace the property you're intested in
          member_issue.issues = issues;
          // 4. Put it back into our array. N.B. we *are* mutating the array here, but that's why we made a copy first
          members[i] = member_issue;
          // 5. Set the state to our new copy
          this.setState({members});
        })
      }
    }
 
    render() {
      let total_issues = 0;
      for(let i = 0; i < this.state.members.length; i++) {
        total_issues += this.state.members[i].issues;
      }
      return (
        <div>
              <div className="cards">
                { this.state.members.map((person, i) => 
                  <div  key={i} className="ordering">
                      <Card person = {person}/>
                  </div>
                )}
              </div>
              <div className="center">
                <Details totalCommits={this.state.totalCommits} totalIssues={total_issues}/>
                <API />
                <Tools />
              </div>
        </div>
      )
    }
}

export default About