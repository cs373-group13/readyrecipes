import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles({
    card: {
      maxWidth: 345,
      minWidth: 200,
    },
    media: {
      height: 200,
    },
    root: {
      flexGrow: 1,
    }
  });

  const details = [
    {
      name: "GitLab Repository",
      source: "https://gitlab.com/cs373-group13/readyrecipes",
      img_url: "https://imgur.com/pnRrmqQ.png"
    },
    {
      name: "API Documentation",
      source: "https://documenter.getpostman.com/view/14748973/TzJrDKyn",
      img_url: "https://imgur.com/bhki1eI.png"
    }
  ];
  

export default function DetailsInfo(props: any) {
  const classes = useStyles();

  return (
    <Container maxWidth="md">
      <Typography variant="h3" gutterBottom>
        About
        <Typography gutterBottom component="h2">
          The motivation for this project comes following a period of quarantine because of the Covid-19 pandemic; large changes such as not
          being able to go to the gym, restaurants, or any public space resulted in many people forced to adapt to new lifestyles. As a result, Ready Recipe
          hopes to mend these changed lifestyles by providing a clear directory of recipes and cuisines, no matter the location of the user. Ready Recipes
          also provides users with information about backyard fruits and vegetables that can be grown depending on where the user is located: these foods
          are also occasionally ingredients in the wide variety of recipes our website provides.
        </Typography>
      </Typography>
      <Grid container className={classes.root} spacing={4} justify="center">
        <Grid item justify="center">
          <Card className={classes.card}>
            <CardActionArea href='https://www.youtube.com/watch?v=WLZxFt9YdDg'>
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Presentation
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={4}>
            {details.map((detail: any) => (
              <Grid key={detail} item>
                <Card className={classes.card}>
                  <CardActionArea href={detail.source}>
                    <CardMedia
                      className={classes.media}
                      image={detail.img_url}
                      title={detail.name}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {detail.name}
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                        {detail.description}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
      <Grid container className={classes.root} spacing={4} justify="center">
        <Grid item xs={4}>
          <Card className={classes.card}>
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Total Commits: {props.totalCommits}
                </Typography>
              </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card className={classes.card}>
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Total Issues: {props.totalIssues}
                </Typography>
              </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
