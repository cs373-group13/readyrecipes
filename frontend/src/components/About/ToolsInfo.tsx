export default function getTools() {
    return [
      {
        name: "Namecheap",
        source: "https://www.namecheap.com",
        img_url: "https://imgur.com/lLpx0o5.png",
        description: "Domain hosting"
      },
      {
        name: "Flask",
        source: "https://palletsprojects.com/p/flask/",
        img_url: "https://imgur.com/7DawxKp.png",
        description: "API Framework"
      },
      {
        name: "Axios",
        source: "https://axios-http.com",
        img_url: "https://imgur.com/Dyz3FVP.png",
        description: "HTTP Client"
      },
      {
        name: "Gunicorn",
        source: "https://gunicorn.org",
        img_url: "https://imgur.com/1aHW6ZK.png",
        description: "WSGI Server"
      },
      {
        name: "SQL Alchemy",
        source: "https://www.sqlalchemy.org",
        img_url: "https://imgur.com/jSVxOjK.jpg",
        description: "SQL wrapper for python"
      },
      {
        name: "Marshmallow",
        source: "https://marshmallow.readthedocs.io/en/stable/#",
        img_url: "https://imgur.com/fyPBirm.png",
        description: "Schema creation"
      },
      {
        name: "Postgres",
        source: "https://www.postgresql.org",
        img_url: "https://imgur.com/KOTH2TN.png",
        description: "Relational database system"
      },
      {
        name: "Selenium",
        source: "https://www.selenium.dev",
        img_url: "https://i.imgur.com/Rxu9SCE.png",
        description: "UI testing framework"
      },
      {
        name: "Black",
        source: "https://pypi.org/project/black/",
        img_url: "https://imgur.com/WVGg1KX.png",
        description: "Python formatter"
      },
      {
        name: "AWS",
        source: "https://aws.amazon.com",
        img_url: "https://imgur.com/uPIZ6wa.png",
        description: "Storage and hosting"
      },
    ]
}
