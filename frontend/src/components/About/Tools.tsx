import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import getTools from './ToolsInfo';

const useStyles = makeStyles({
    card: {
      maxWidth: 400,
      minWidth: 200,
    },
    media: {
      height: 200,
      minWidth: 200,
      // objectFit: 'fill'
    },
    root: {
      flexGrow: 1,
    }
  });

export default function ToolsGrid() {
  const classes = useStyles();
  const tools = getTools();

  return (
    <div>
      <Typography variant="h3" gutterBottom>
        Development Tools
      </Typography>
      <Grid container className={classes.root} spacing={4}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={4}>
            {tools.map((tool: any) => (
              <Grid key={tool} item>
                <Card className={classes.card}>
                  <CardActionArea href={tool.source}>
                    <CardMedia
                      className={classes.media}
                      image={tool.img_url}
                      title={tool.name}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {tool.name}
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                        {tool.description}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
