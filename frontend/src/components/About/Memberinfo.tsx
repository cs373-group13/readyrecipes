const Memberinfo = [
    {
        name : "Remus Wong",
        committer_name: "RemusW",
        email : "rwong2100@hotmail.com",
        commits: 0,
        issues: 0,
        key: 1,
        unitTests: 5,
        picture: './Photos/remus.jpg',
        bio: 'Currently a junior at the University of Texas at Austin, Remus Wong studies Computer Science and is working on a certificate in Digital Arts and Media. You can catch him playing around with 3D software, playing any competitive video game, or trying to finish the latest Stormlight Archive book.'
    },
    {
        name : "Daniel Young",
        committer_name: "danyoungday",
        email : "danyoung@utexas.edu",
        commits: 0,
        issues: 0,
        key: 2,
        unitTests: 10,
        picture: './Photos/daniel.jpg',
        bio: 'Daniel is a junior in Computer Science at UT Austin who enjoys sleeping, running, and camping. Fun fact: the longest stretch he has  spent away from civilization was 26 days straight in Death Valley.'
    },
    {
        name : "Timothy Situ",
        committer_name: "timothysitu",
        email : "55562117+timothysitu@users.noreply.github.com",
        commits: 0,
        issues: 0,
        key: 3,
        unitTests: 4,
        picture: './Photos/timothy.jpg', 
        bio: 'Timothy is a third year CS major at UT Austin. He is from Plano, Texas, and he enjoys listening to new music, being in nature, and finding cool jewelry to wear.'
    },
    {
        name : "Tejna Dasari",
        committer_name: "tejnadas",
        email : "dasaritejna@gmail.com",
        commits: 0,
        issues: 0,
        key: 4,
        unitTests: 2,
        picture: './Photos/tejna.jpg', 
        bio: 'Tejna is a Junior Computer Science Major. She is from Pearland, Texas. In her free time, she enjoys riding her bike and watching movies!'
    },
    {
        name : "Ian Trowbridge",
        committer_name: "trowk",
        email : "trowbridge.iank@protonmail.com",
        commits: 0,
        issues: 0,
        key: 5,
        unitTests: 8,
        picture: './Photos/ian.jpg', 
        bio: 'Ian is a Junior double majoring in Computer Science and Math. In his free time, he enjoys playing games like Hollow Knight and reading fantasy novels.'
    }
]

export default Memberinfo
