import React from "react"
import {AllRelatedSections, relatedObjI, ModelType} from "../templates/RelatedSection"
import { RouteComponentProps, Link } from "react-router-dom";
import Loading from '../Loading/Loading'
import axios from "axios"
import { CardMedia, Container, Grid} from '@material-ui/core';
import { Card, CardContent, CardActionArea} from '@material-ui/core';
import MapSection from '../templates/MapSection';
import { Container as BootContainer, Card as BootCard, ListGroup, ListGroupItem} from 'react-bootstrap';


interface RouteParams {
  id: string
}

interface GeoInstanceI extends RouteComponentProps<RouteParams> {
}

interface GeoInstanceStateI {
  data: any;
  id: number;
  loading: boolean;
  relatedRecipes: relatedObjI
  relatedCrops: relatedObjI
  photoLink: string
}

class GeoInstance extends React.Component<GeoInstanceI, GeoInstanceStateI> {
  constructor(props: GeoInstanceI) {
    super(props);
    const url = window.location.href;
    const split_url = url.split("/");
    const id = Number(split_url[split_url.length - 1]);
    console.log("Instance constructor");
    console.log(url);
    console.log(id);

    this.state = {data: undefined, id: id, loading: true,
                  relatedRecipes: {} as relatedObjI, relatedCrops: {} as relatedObjI, photoLink: ''}
  }

  async componentDidMount() {
    console.log("Requesting API")
    console.log(this.state.id)
    const [geolocationInstance, relatedRecipes, relatedCrops] = await Promise.all([
      axios.get(`${process.env.REACT_APP_API_URL}/geolocation/${this.state.id}`),
      axios.get(`${process.env.REACT_APP_API_URL}/city_to_recipes/${this.state.id}`),
      axios.get(`${process.env.REACT_APP_API_URL}/city_to_vegetables/${this.state.id}`)
    ]);
    const relatedRecipesObj: relatedObjI = {data: relatedRecipes.data,
      header:"Related Recipes",
      model_type: ModelType.Recipe};
    const relatedCropsObj: relatedObjI = {data: relatedCrops.data,
        header:"Related Vegetables",
        model_type: ModelType.Crop};
    this.setState({ 
      data: geolocationInstance.data,
      relatedRecipes:relatedRecipesObj,
      relatedCrops: relatedCropsObj,
      loading: false,
    });
  }
  
  render() {
    const data:any = this.state.data;
    const relatedRecipes: relatedObjI = this.state.relatedRecipes;
    const relatedCrops: relatedObjI = this.state.relatedCrops;
    if (!this.state.loading) {
      console.log(`https://maps.google.com/?q=${data["lat"]},${data["lon"]}`);
    }
    return (
      this.state.loading ? <Loading /> :
      <Container maxWidth="lg">
      <p></p>
      <div>
        <Container maxWidth="md">
          <Grid container spacing={4}>
            <Grid item xs >
              <Card style={{ textAlign: "center" }}>
                <CardActionArea href={"http://www.wikipedia.org/wiki/" + data["name"]}>
                  <CardMedia style={{ height: "250px", paddingTop: "2%" }}
                    image="https://i.imgur.com/NdzteMG.png"
                    title="Wikipedia" />
                </CardActionArea>
                <CardContent>
                  <a id="link" href={"http://www.wikipedia.org/wiki/" + data["name"]}>Wikipedia</a>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs >
              <BootContainer fluid>
                  <MapSection location={{
                    lat: Number(data.lat),
                    lng: Number(data.lon)
                  }} zoomLevel={10} mapTypeId="hybrid" />
              </BootContainer>
            </Grid>
          </Grid>
        </Container>
        <p></p>
        <BootContainer fluid>
          <BootCard style={{height:'100%', width: "100%"}}>
            <BootCard.Header><h1 style={{textAlign: 'center'}}>{data["name"]}</h1></BootCard.Header>
              <ListGroup>
                <h3>Coordinates:</h3>
                <ListGroupItem> Latitude: {data["lat"]}</ListGroupItem>
                <ListGroupItem> Longitude: {data["lon"]} </ListGroupItem>
                <h3>Weather:</h3>
                <ListGroupItem> {data["weather"][0]["main"]}: {data["weather"][0]["description"]} </ListGroupItem>
                <ListGroupItem> Temperature in Celsius: {Math.round(data["temp"] - 273.15)} </ListGroupItem>
                <ListGroup horizontal>
                  <ListGroup.Item className='flex-fill'> Min Temperature: {Math.round(data["temp_min"] - 273.15)} </ListGroup.Item>
                  <ListGroup.Item className='flex-fill'> Max Temperature: {Math.round(data["temp_max"] - 273.15)} </ListGroup.Item>
                </ListGroup>
                <ListGroupItem> Pressure: {data["pressure"]} hpa </ListGroupItem>
                <ListGroupItem> Humidity: {data["humidity"]}% </ListGroupItem>
              </ListGroup>
          </BootCard>
        </BootContainer>
        <p></p>
        <Container>
          <AllRelatedSections related1={relatedRecipes} related2={relatedCrops}></AllRelatedSections>
          <Link to='/geolocation'>Back</Link>
        </Container>
      </div>
      </Container>
    );
  }
}

export default GeoInstance;
