import React from "react"
import MUIDataTable from "mui-datatables";
import CustomRow from "../templates/CustomRow"

interface ITableProp {
  data: any[];
  initSearch?:string;
}

interface ITableState {
  searchText: string|null;
  showColumns: boolean[];
  columns: any[];
  options: any;
}
  
  class GeoTable extends React.Component<ITableProp, ITableState> {
    dict = {"id": 0, "name": 1, "humidity": 2, "lat": 3, "lon": 4, "temp": 5}
    constructor(props: ITableProp) {
      super(props);
      const initSearch = this.props.initSearch === undefined ? null : this.props.initSearch;
      this.onSearchChange = this.onSearchChange.bind(this);
      this.customRowRender = this.customRowRender.bind(this);
      this.onViewColumnsChange = this.onViewColumnsChange.bind(this);

      const columns = [
        {
          name: "id",
          label: "Geolocation ID",
          options: {
            sort: true,
            filter: false,
            display: false,
            searchable: false
          },
        },
        { name: "name", label: "City" },
        { name: "humidity", label: "Humidity" },
        { name: "lat", label: "Latitude" },
        { name: "lon", label: "Longitude" },
        { name: "temp", label: "Temperature in Celsius" },
      ];
      
      const options = {
        jumpToPage: true,
        print: false,
        download: false,
        selectableRowsHideCheckboxes: true,
        onRowClick: (data: any) =>
          window.location.assign("/geolocationInstance/" + data[0]),
        onSearchChange: this.onSearchChange,
        customRowRender: this.customRowRender,
        onViewColumnsChange: this.onViewColumnsChange,
        searchText: initSearch
      };
      const showColumns:boolean[] = [false, true, true, true, true, true]
        
      this.state = {searchText: initSearch, columns: columns, options: options, showColumns: showColumns}
    }

    onSearchChange(searchText: string) {
      console.log(searchText);
      this.setState({searchText: searchText});
    }

    onViewColumnsChange(changedColumn: string, action: string) {
      const shownColumns: boolean[] = this.state.showColumns;
      shownColumns[this.dict[changedColumn]] = "add" === action;
      this.setState({
        showColumns: shownColumns
      })
    }

    onRowClick(id: number) { 
      window.location.assign("/geolocationInstance/" + id);
    }

    customRowRender(data, dataIndex, rowIndex) {
      const searchText:string|null = this.state.searchText;
      const showColumns: boolean[] = this.state.showColumns;
      return <CustomRow 
                      searchText={searchText}
                      data = {data}
                      showColumns={showColumns}
                      onRowClick={this.onRowClick}
              />
                    
    }

    render() {
      const data: any = this.props.data;
      const columns: any[] = this.state.columns;
      const options: any = this.state.options;
      return (
        <MUIDataTable
          title="Geolocations"
          data={data}
          columns={columns}
          options={options}
        />
  
      );
    }
  }

  export default GeoTable;
