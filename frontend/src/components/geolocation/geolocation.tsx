import React from "react"
import  {Geolocation} from './geolocationInterface';
import GeoTable from "./geolocationTable"
import Loading from '../Loading/Loading'
import "./geolocation.css"
import axios from "axios"
import { Container } from '@material-ui/core';


// Geolocation model
interface IProp {
  initSearch?: string,
}

interface IState {
  pageData: Geolocation[];
  currentPage?: number;
  numPerPage:number;
  totalPages?: number;
  records?: number;
  loading: boolean;
}

class GeolocationModel extends React.Component<IProp, IState> {
  constructor(props: IProp) {
    super(props);
    this.state = {pageData: [], currentPage: undefined, numPerPage: 20,
      totalPages: undefined, records: undefined, loading: true};
    // this.onPageChanged = this.onPageChanged.bind(this);
  }

  componentDidMount() {
    console.log("Requesting API")
    axios.get(`${process.env.REACT_APP_API_URL}/geolocation`)
    .then(response => {
      const currGeolocations = response.data.data;
      currGeolocations.forEach( geoloc => {
        geoloc.temp = Math.round(geoloc.temp - 273.15);
      });
      this.setState({ 
                    pageData: currGeolocations,
                    currentPage: 1,
                    totalPages: response.data.num_pages,
                    records: response.data.records,
                    loading: false
                  });
      console.log("Retrievd API");
    })
    .catch(error => {
      // keep for jest test
    });
  }
  

  render() {
    return (
      this.state.loading ? 
      <div>
        <header className="citiesImage">
            <h1 className="citiesHeader">Geolocations</h1>
            <h4 className="citiesH4">Search the world!</h4>
        </header>
        <Loading />
      </div> :
      <div>
        <header className="citiesImage">
            <h1 className="citiesHeader">Geolocations</h1>
            <h4 className="citiesH4">Search the world!</h4>
        </header>
        <Container maxWidth="xl">
            <GeoTable data={this.state.pageData} initSearch={this.props.initSearch}/>
        </Container>
      </div>
    );
  }
}

export default GeolocationModel