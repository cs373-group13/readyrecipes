import React, { Component } from 'react'
import { Spinner, Container, Row } from 'react-bootstrap'

class Loading extends Component {
    
    render() {

        const messages = [
            "Watching Grass Grow...",
            "Watering the Garden...",
            "Checking the Weather...",
            "Grabbing a snack...",
            "Counting Calories...",
            "Taking a Cheat Day...",
            "Keeping the Doctor Away..."

        ]
        var message = messages[Math.floor(Math.random() * messages.length)];

        return(
            <Container fluid>
                <Row className='justify-content-center'>
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </Row>
                <Row className='justify-content-center' style={{textAlign:'center'}}>
                    <p>{message}</p>
                </Row>
            </Container>
        );
    }
}

export default Loading