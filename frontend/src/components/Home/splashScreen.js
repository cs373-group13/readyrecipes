import React, {Component} from 'react';


class SplashScreen extends Component {
    render(){
        return(
            <p>
                Welcome to Ready Recipes!
            </p>
        );
    }
}
export default SplashScreen
/*
Saving this for later when we do loading
class MainComponent extends React.Component {
    constructor(props){
        this.state = {
            renderSplashScreen: true
        };
    }

    apiCallback(data) {
        this.setState({renderSplashScreen: false});
    }

    render(){
        let view; 
        if(this.state.renderSplashScreen){
            return <SplashScreen/>
        } else {
            return <OtherComponent/>
        }
    }
}
*/