import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './Home.css';
import SearchBar from "../Search/SearchBar"


class Home extends Component {
  render() {
    return(
      <div className='bg-image'>
        <Container fluid>
          {/*I know this is terrible but I don't know how to do it otherwise*/}
          <Row/>
          <Row/>
          <Row/>
          <Row/>
          <Row/>
          <Row/>
          <Row/>
          <Row/>
          <Row/>
          <Row>
            <Col>
              <h1 className='center'>Bringing Ready Recipes straight to your kitchen.</h1>
            </Col>
          </Row>
          <Row>
            <p className='center'>Cook recipes with crops you grow in your own backyard.</p>
          </Row>
            <SearchBar query={""} />
        </Container>
      </div>
    );
  }
}

export default Home