import { Link } from "react-router-dom";
import "./card-style.css"
import React from "react"
import Highlight from "../templates/Highlight";
import { CardMedia, Grid, Typography } from '@material-ui/core';
import { Card, CardContent, CardActionArea } from '@material-ui/core';


interface cropPropI {
    data: any[];
    searchText: string;
  }
  
class CropsTable extends React.Component<cropPropI> {
    constructor(props: cropPropI) {
        super(props);
        this.getCard = this.getCard.bind(this);
    }
      getCard({data, index}: any) {
        const searchText = this.props.searchText;
        const year: string = data['year'] === null ? '' : data['year'].toString();
        return (
            <Grid item xs={2} key={index} >
                <Link to={`/gardenInstance/${data['id']}`} style={{ textDecoration: 'none' }}>
                    <CardActionArea style={{ height: "100%" }}>
                        <Card style={{ height: "100%", display: "flex", flexDirection: "column" }}>
                            <CardMedia image={data['image_url']}
                                    title={data['common_name']} 
                                    style={{height: "200px"}}/>
                            <CardContent style={{ flexGrow: 0 }}>                                
                                <Typography variant="h5" component="h2">
                                    <Highlight word={data['common_name']} searchText={searchText}></Highlight>
                                </Typography>
                                <Typography display="inline">Genus: </Typography>
                                <Typography display="inline">
                                    <Highlight word={data['genus']} searchText={searchText}></Highlight>
                                </Typography>
                                <br/>
                                <Typography display="inline">Family: </Typography>
                                <Typography display="inline">
                                    <Highlight word={data['family']} searchText={searchText}></Highlight>
                                </Typography>
                                <br/>
                                <Typography display="inline">Family Common Name: </Typography>
                                <Typography display="inline">
                                    <Highlight word={data['family_common_name']} searchText={searchText}></Highlight>
                                </Typography>
                                <br/>
                                <Typography display="inline">Year Classified: </Typography>
                                <Typography display="inline">
                                    <Highlight word={year} searchText={searchText}></Highlight>
                                </Typography>
                            </CardContent>
                        </Card>
                    </CardActionArea>
                </Link>`
            </Grid>
        );
      }


    render() {
        return (
                <Grid container spacing={2} alignItems="stretch">
                    {this.props.data.map((datum, index) => {
                        return this.getCard({data: datum, index: index})
                    })}
                </Grid>
        );
    }
}

export default CropsTable;