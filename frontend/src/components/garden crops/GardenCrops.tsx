import { Component } from "react"
import {PaginationDataI, Paginatior} from "../templates/Paginator"
import CropsTable from "./cropsTable"
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { Container, Grid, Typography } from '@material-ui/core';
import Loading from '../Loading/Loading'
import axios from "axios"
// import dummyData from "./gardencrops.json"

import { keyComparator } from "./CropsSort"

const sortVals = [
  {'name': 'id', 'label': 'Default', 'val': ['id', 'ascending']},
  {'name': 'common_name', 'label': 'Name Descending', 'val': ['common_name', 'descending']},
  {'name': 'common_name', 'label': 'Name Ascending', 'val': ['common_name', 'ascending']},
  {'name': 'genus', 'label': 'Genus Descending', 'val': ['genus', 'descending']},
  {'name': 'genus', 'label': 'Genus Ascending', 'val': ['genus', 'ascending']},
  {'name': 'family', 'label': 'Family Descending', 'val': ['family', 'descending']},
  {'name': 'family', 'label': 'Family Ascending', 'val': ['family', 'ascending']},
  {'name': 'family_common_name', 'label': 'Common Family Descending', 'val': ['family_common_name', 'descending']},
  {'name': 'family_common_name', 'label': 'Common Family Ascending', 'val': ['family_common_name', 'ascending']},
  {'name': 'year', 'label': 'Year Classified Descending', 'val': ['year', 'descending']},
  {'name': 'year', 'label': 'Year Classified Ascending', 'val': ['year', 'ascending']}
];

const filters = [
  {'name': 'common_name', 'label': 'Name'},
  {'name': 'genus', 'label': 'Genus'},
  {'name': 'family', 'label': 'Family'},
  {'name': 'family_common_name', 'label': 'Common Family Name'},
  {'name': 'year', 'label': 'Year Classified'},
];

const PAGE_LIMIT = 12;

interface IProp {
  initSearch?: string,
}

interface IState {
  allData: any[];
  pageData: any[];
  currentPage: number;
  totalPages: number;
  loading: boolean;
  searchText: string;
  sortBy: string;
  filter: any;
  filterData: any[];
}
const BASE_FILTER = [["common_name", "All"], ["genus", "All"], ["family", "All"],
                     ["family_common_name", "All"], ["year", "All"]]

class GardenCrops extends Component<IProp, IState> {
    keys = ['common_name', 'genus', 'family', 'family_common_name', 'year'];
    constructor(props: IProp) {
        super(props);
        const initSearch = this.props.initSearch === undefined ? "" : this.props.initSearch;
        console.log("GARDEN CROPS CONSTRUCTOR: ", initSearch, this.props.initSearch);
        this.state = {allData: [], pageData: [], currentPage: 1, loading: true,
            searchText: initSearch, filterData: [], totalPages: 0,
            filter: BASE_FILTER, sortBy: ['id', 'ascending'] + ''};
          this.onPageChanged = this.onPageChanged.bind(this);
          this.handleSearch = this.handleSearch.bind(this);
          this.handleSort = this.handleSort.bind(this);
          this.handleFilter = this.handleFilter.bind(this);
      }

      componentDidMount() {
        console.log("Requesting API")
        axios.get(`${process.env.REACT_APP_API_URL}/gardencrops`)
        .then(response => {
          const gardenCrops = this.sort(response.data.data, this.state.sortBy);
          const searchText = this.state.searchText;
          let filterData = gardenCrops;
          if (this.state.searchText !== "") {
            filterData = gardenCrops.filter(record => {
              return (
                (record['common_name'] !== null && record['common_name'].toLowerCase().includes(searchText)) ||
                (record['genus'] !== null && record['genus'].toLowerCase().includes(searchText)) ||
                (record['family'] !== null && record['family'].toLowerCase().includes(searchText)) ||
                (record['family_common_name'] !== null && record['family_common_name'].toLowerCase().includes(searchText)) ||
                (record['year'] !== null && record['year'].toString().toLowerCase().includes(searchText))
              );
            });
          }
        const totalPages = Math.ceil(filterData.length / PAGE_LIMIT);
          this.setState({ 
                        allData: gardenCrops,
                        filterData: filterData,
                        pageData: filterData.slice(0, PAGE_LIMIT),
                        totalPages: totalPages,
                        currentPage: 1,
                        loading: false
                      });
          console.log("Retreived API");
        })
        .catch(error => {
          // keep for jest test
        });
      }
      
      onPageChanged = (pageInfo: PaginationDataI) => {
        const { currentPage } = pageInfo;
        const lower = (currentPage-1) * PAGE_LIMIT;
        const upper = lower + PAGE_LIMIT;
        const pageData = this.state.filterData.slice(lower, upper)
        this.setState({ 
          pageData: pageData,
          currentPage: currentPage,
          loading: false
        });
      }

      handleSearch = (event) => {
        let searchTextI: string = event.target.value;
        console.log("Search: ", searchTextI);
        let searchText = searchTextI.toLowerCase();

        let data = this.filter(this.state.allData, this.state.filter);
        data = this.sort(data, this.state.sortBy)

        data = data.filter(record => {
          return (
            (record['common_name'] !== null && record['common_name'].toLowerCase().includes(searchText)) ||
            (record['genus'] !== null && record['genus'].toLowerCase().includes(searchText)) ||
            (record['family'] !== null && record['family'].toLowerCase().includes(searchText)) ||
            (record['family_common_name'] !== null && record['family_common_name'].toLowerCase().includes(searchText)) ||
            (record['year'] !== null && record['year'].toString().toLowerCase().includes(searchText))
          );
        });


        this.setState({
          searchText: searchTextI,
          filterData: data,
          pageData: data.slice(0, PAGE_LIMIT),
          totalPages: Math.ceil(data.length / PAGE_LIMIT),
          currentPage: 1
        });
      }

      sort(data: any[], sortText: string) {
        console.log("SORTING")
        let [name, ascending] = sortText.split(",");
        if (ascending !== 'ascending') {
          name = '-' + name;
        }
        return data.sort(keyComparator(name))
      }

      filter(data: any[], filter: any[]) {
        for (let i = 0; i < filter.length; i++) {
          if (filter[i][1] !== "All") {
            data = data.filter(crop => {
              return crop[filter[i][0]] === null ? 
                     crop[filter[i][0]] === filter[i][1] :
                     crop[filter[i][0]].toString() === filter[i][1].toString();
            });
          }
        }
        return data
      }

      handleSort = (event) => {
        console.log("SORTING")
        let data = this.filter(this.state.allData, this.state.filter);
        data = this.sort(data, (event.target.value as string))

        this.setState({
          searchText: '',
          sortBy: (event.target.value as string),
          filterData: data,
          pageData: data.slice(0, PAGE_LIMIT),
          currentPage: 1
        });
      }

      handleFilter = (event) => {
        const [name, filterVal] = (event.target.value as string).split(",");
        console.log('FILTER', name, filterVal);
        let data = this.state.allData;

        let filter = this.state.filter;

        for (let i = 0; i < filter.length; i++) {
          if (filter[i][0] === name) {
            filter[i][1] = filterVal;
          }
          if (filter[i][1] !== "All") {
            data = data.filter(crop => {
              return crop[filter[i][0]] === null ? 
                     crop[filter[i][0]] === filter[i][1] :
                     crop[filter[i][0]].toString() === filter[i][1].toString();
            });
          }
        }
        data = this.sort(data, this.state.sortBy);

        this.setState({
          searchText: '',
          filterData: data,
          filter: filter,
          pageData: data.slice(0, PAGE_LIMIT),
          totalPages: Math.ceil(data.length / PAGE_LIMIT),
          currentPage: 1
        });
      }

      getSearchBar() {
        return (
            <Grid item xs={4}>
              <TextField 
                        id="standard-basic"
                        label="Search"
                        onChange={this.handleSearch}
                        value={this.state.searchText}
                        style={{ width: '100%'}}
              />
            </Grid>
        );
    }

    getMenu() {
      return (
        <Grid item xs={4}>
          <FormControl variant="outlined" style={{ width: '100%'}}>
            <InputLabel id="demo-simple-select-outlined-label">Sort by</InputLabel>
            <Select label="Sort by" labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined" value={this.state.sortBy} onChange={this.handleSort}>
              {sortVals.map((sortVal, idx) => (
                <MenuItem key={idx} value={sortVal.val + ''}>
                  {sortVal.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      );
    }
  
    getFilter(filter: any, idx: number) {
      const data = this.state.allData;
      const unique = Array.from(new Set(data.map(item => item[filter.name]))).sort();
      unique.unshift("All")
      return (
        <Grid item xs={2}>
        <FormControl variant="outlined" style={{ width: '100%'}}>
          <InputLabel id="demo-simple-select-outlined-label">{filter.label}</InputLabel>
          <Select label="Filter by" labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined" value={this.state.filter[idx] + ''} onChange={this.handleFilter}>
            {unique.map((val, i) => (
              <MenuItem key={i} value={[filter.name, val] + ''}>
                {val}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        </Grid>
      );
    }
  
    getFilters() {
      return filters.map((filter, idx) => (
        this.getFilter(filter, idx)
      ))
    }

    render() {
        const totalCrops:number = this.state.filterData === undefined ? 0 : this.state.filterData.length;
        const displayedRecords: number = this.state.pageData === undefined ? 0 : this.state.pageData.length;
        console.log('RENDER TABLE', totalCrops);
        return(
            this.state.loading ? 
            <div>
              <Typography component="h1" variant="h2" align="center" color="textPrimary">
                Garden Crops
              </Typography>
              <Loading />
            </div> :
             <Container maxWidth="xl">
                <Typography component="h1" variant="h2" align="center" color="textPrimary">
                  Garden Crops
                </Typography>
                <Grid container spacing={2} justify="space-between">
                  {this.getSearchBar()}
                  {this.getMenu()}
                </Grid>
                <br />
                <Grid container spacing={2} justify="space-between">
                  {this.getFilters()}
                </Grid>
                <br />
                  <CropsTable data={this.state.pageData} searchText={this.state.searchText}/>
                <br />
                  <Grid container justify="space-between">
                    <Typography>Total Records: {totalCrops}</Typography>
                    <Paginatior key='cropPaginator'
                        currPage={this.state.currentPage}
                        totalRecord={totalCrops}
                        totalPages={this.state.totalPages}
                        pageLimit={PAGE_LIMIT}
                        pageNeighbours={2}
                        onPageChanged={this.onPageChanged} 
                    />
                    <Typography>Displayed:{displayedRecords}</Typography>
                  </Grid>
            </Container>
        );
    }
}

export default GardenCrops;
