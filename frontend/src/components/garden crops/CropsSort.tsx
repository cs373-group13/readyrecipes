export function keyComparator(property) {
    var sortOrder = 1;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a,b) {
        const ap = a[property] !== null ? a[property] : property === 'year' ||  property === 'id'?
                                                        0 : '';
        const bp = b[property] !== null ? b[property] : property === 'year' ||  property === 'id' ?
                                                        0 : '';
        if(sortOrder === -1){
            return property !== 'year' &&  property !== 'id' ? bp.localeCompare(ap) :
                                         bp - ap;
        }else{
            return property !== 'year' &&  property !== 'id' ? ap.localeCompare(bp) :
                                         ap - bp;
        }        
    }
}