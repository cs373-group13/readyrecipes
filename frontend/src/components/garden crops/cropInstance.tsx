import React from "react"
import { RouteComponentProps } from "react-router-dom";
import {AllRelatedSections, relatedObjI, ModelType} from "../templates/RelatedSection"
import Loading from '../Loading/Loading'
import axios from "axios"
import { CardMedia, Container, Grid } from '@material-ui/core';
import { Card, CardContent, CardActionArea } from '@material-ui/core';
import { Container as BootContainer, Card as BootCard, ListGroup, ListGroupItem} from 'react-bootstrap';


interface RouteParams {
    id: string
  }
  
  interface cropPropI extends RouteComponentProps<RouteParams> {
  }
  

interface CropInstanceStateI {
  data: any;
  id: number;
  loading: boolean;
  relatedGeolocations: relatedObjI
  relatedRecipes: relatedObjI
}

class CropInstance extends React.Component<cropPropI, CropInstanceStateI> {
    constructor(props: cropPropI) {
      console.log("Constructor");
      super(props);
      const id:number = Number(props.match.params.id);
      
      this.state = {data: undefined, id: id, loading: true,
        relatedGeolocations: {} as relatedObjI, relatedRecipes: {} as relatedObjI}
      console.log(this.state);
    }
    async componentDidMount() {
      console.log("Requesting API")
      console.log(this.state.id)
  
      const [recipeInstance, relatedRecipes, relatedGeolocations] = await Promise.all([
        axios.get(`${process.env.REACT_APP_API_URL}/gardencrops/${this.state.id}`),
        axios.get(`${process.env.REACT_APP_API_URL}/vegetable_to_recipes/${this.state.id}`),
        axios.get(`${process.env.REACT_APP_API_URL}/vegetable_to_cities/${this.state.id}`)
      ]);
      const relatedRecipesObj: relatedObjI = {data: relatedRecipes.data,
        header:"Related Recipes",
        model_type: ModelType.Recipe};
      const relatedGeolocationsObj: relatedObjI = {data: relatedGeolocations.data,
          header:"Related Geolocations",
          model_type: ModelType.Geolocation};
      this.setState({ 
        data: recipeInstance.data,
        relatedGeolocations:relatedGeolocationsObj,
        relatedRecipes: relatedRecipesObj,
        loading: false,
      });
    }
    render() {
        const data:any = this.state.data;
        const relatedGeolocations: relatedObjI = this.state.relatedGeolocations;
        const relatedRecipes: relatedObjI = this.state.relatedRecipes;
        return (
            this.state.loading ? <Loading /> :
            <div>
              <Container>
                <h1>{data['common_name']}</h1>
                <Container maxWidth="md">
                  <Grid container spacing={4}>
                    <Grid item xs >
                      <Card style={{ textAlign: "center" }}>
                        <CardActionArea href={data['image_url']}>
                          <CardMedia style={{ height: "250px", paddingTop: "2%" }}
                            image={data['image_url']}
                            title={data['common_name']} />
                        </CardActionArea>
                        <CardContent>
                          <a id="link" href={data['image_url']}>Image</a>
                        </CardContent>
                      </Card>
                    </Grid>
                    <Grid item xs >
                      <Card style={{ textAlign: "center" }}>
                        <CardActionArea href={"http://www.wikipedia.org/wiki/" + data["genus"]}>
                          <CardMedia style={{ height: "250px", paddingTop: "2%" }}
                            image="https://i.imgur.com/NdzteMG.png"
                            title="Wikipedia" />
                        </CardActionArea>
                        <CardContent>
                          <a id="link" href={"http://www.wikipedia.org/wiki/" + data["genus"]}>Wikipedia</a>
                        </CardContent>
                      </Card>
                    </Grid>
                  </Grid>
                </Container>
                <p></p>
                <BootContainer fluid>
                  <BootCard style={{height:'100%', width: "100%"}}>
                    <BootCard.Header><h1 style={{textAlign: 'center'}}>Information</h1></BootCard.Header>
                      <ListGroup>
                        <ListGroupItem> Family: {data['family']} </ListGroupItem>
                        <ListGroupItem> Common Family Name: {data['family_common_name']} </ListGroupItem>
                        <ListGroupItem> Genus: {data['genus']} </ListGroupItem>
                        <ListGroupItem> Year Classified: {data['year']} </ListGroupItem>
                        {data['bloom_months']==null ? null : <ListGroupItem> Bloom Months: {data['bloom_months'] !== null ? 
                          data['bloom_months'].map((key, index) => {
                            return <li key={index}>{key}</li>
                            }) : "Not included"} </ListGroupItem>}
                        {data['description']==null ? null : <ListGroupItem> Description: {data['description']} </ListGroupItem>} 
                        {data['minimum_temperature']==null ? null : <ListGroupItem> Minimum temperature: {data['minimum_temperature']} fahrenheit </ListGroupItem>} 
                        {data['maximum_temperature']==null ? null : <ListGroupItem> Maximum temperature: {data['maximum_temperature']} fahrenheit </ListGroupItem>} 
                        {data['days_to_harvest']==null ? null : <ListGroupItem> Days to harvest: {data['days_to_harvest']} </ListGroupItem>} 
                        {data['sowing']==null ? null : <ListGroupItem> Sowing: {data['sowing']} </ListGroupItem>}
                        {data['light']==null ? null : <ListGroupItem> Light: {data['light']} (on a scale from 0, no light, to 10, very intensive insolation) </ListGroupItem>}
                      </ListGroup>
                  </BootCard>
                </BootContainer>
                <p></p>
                <AllRelatedSections related1={relatedGeolocations} related2={relatedRecipes}></AllRelatedSections>
              </Container>
            </div>
        );
    }
}

export default CropInstance;