import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { MenuItems } from './MenuItems';
import './Navbar.css';

class Navbar extends Component {
    render() {
        return (
            <nav className="NavbarItems">
                <ul className="NavList">
                    {MenuItems.map((item, index) => {
                        return (
                            <li key={index} className="NavItem">
                                <Link className={item.cName} to={item.url}>
                                    {item.title}
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </nav>
        )
    }
}

export default Navbar