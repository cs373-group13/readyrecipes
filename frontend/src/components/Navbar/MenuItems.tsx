export const MenuItems = [
    {
        title: 'Home',
        url: '/',
        cName: 'nav-links'
    },
    {
        title: 'About',
        url: '/about',
        cName: 'nav-links'
    },
    {
        title: 'Geolocations',
        url: '/geolocation',
        cName: 'nav-links'
    },
    {
        title: 'Recipes',
        url: '/recipes',
        cName: 'nav-links'
    },
    {
        title: 'Garden Crops',
        url: '/gardencrops',
        cName: 'nav-links'
    },
    {
        title: 'Our Visualizations',
        url: '/visualizations',
        cName: 'nav-links'
    },
    {
        title: 'Provider Visualizations',
        url: '/providervisualizations',
        cName: 'nav-links'
    },
    {
        title: 'Search',
        url: '/search',
        cName: 'nav-links'
    }
]