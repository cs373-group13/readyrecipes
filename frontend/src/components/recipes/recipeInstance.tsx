import React from "react"
import { RouteComponentProps, Link } from "react-router-dom";
import {AllRelatedSections, relatedObjI, ModelType} from "../templates/RelatedSection"
import { Container, Row, ListGroup, Card } from 'react-bootstrap'
import Loading from "../Loading/Loading"
import axios from "axios"
import { CardMedia } from '@material-ui/core';
import { CardContent, CardActionArea } from '@material-ui/core';


interface RouteParams {
  id: string
}

interface RecipeInstanceI extends RouteComponentProps<RouteParams> {
}

interface RecipeInstanceStateI {
  data: any;
  id: number;
  loading: boolean;
  relatedGeolocations: relatedObjI
  relatedCrops: relatedObjI
}

class RecipeInstance extends React.Component<RecipeInstanceI, RecipeInstanceStateI> {
  constructor(props: RecipeInstanceI) {
    super(props);
    const url = window.location.href;
    const split_url = url.split("/");
    const id = Number(split_url[split_url.length - 1]);

    this.state = {data: undefined, id: id, loading: true,
        relatedGeolocations: {} as relatedObjI, relatedCrops: {} as relatedObjI}
  }
    

  async componentDidMount() {
    console.log("Requesting API")
    console.log(this.state.id)
    const [recipeInstance, relatedCities, relatedCrops] = await Promise.all([
      axios.get(`${process.env.REACT_APP_API_URL}/recipes/${this.state.id}`),
      axios.get(`${process.env.REACT_APP_API_URL}/recipe_to_cities/${this.state.id}`),
      axios.get(`${process.env.REACT_APP_API_URL}/recipe_to_vegetables/${this.state.id}`)
    ]);
    const relatedGeolocationsObj: relatedObjI = {data: relatedCities.data,
      header:"Related Geolocations",
      model_type: ModelType.Geolocation};
    const relatedCropsObj: relatedObjI = {data: relatedCrops.data,
        header:"Related Vegetables",
        model_type: ModelType.Crop};
    this.setState({ 
      data: recipeInstance.data,
      relatedGeolocations:relatedGeolocationsObj,
      relatedCrops: relatedCropsObj,
      loading: false,
    });
  }

  capitalize(str)
  {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }
    

  GetIngredient(ingredient:any) {
    return (
      <ListGroup.Item>
        {this.capitalize(ingredient['name'])}: {ingredient['amount']} {ingredient['unit'] !== null ? ingredient['unit'] : ""}
      </ListGroup.Item>
    );
  }

  GetIngredients(ingredients:any[]) {
    var ings = ingredients.map((key, index) => this.GetIngredient(key));
    return(
      <ListGroup variant="flush">
        {ings}
      </ListGroup>
    );
  }

  GetInstruction(instruction:any, index: number) {
    return (
      <ListGroup.Item>
        {index+1}: {instruction['step']}
      </ListGroup.Item>
    );
  }

  GetInstructions(instructions:any[]) {
    var insts = instructions.map((key, index) => this.GetInstruction(key, index));
    return(
      <ListGroup variant="flush">
        {insts}
      </ListGroup>
    );
  }
  
  render() {
    const data:any = this.state.data;
    const relatedGeolocations: relatedObjI = this.state.relatedGeolocations;
    const relatedCrops: relatedObjI = this.state.relatedCrops;

    return (
      this.state.loading ? <Loading /> :
      <Container fluid>
        <Row className="justify-content-center">
          <Card style={{width: "50%"}}>
            <Card.Header><h2 style={{textAlign: 'center'}}>{data["title"]}</h2></Card.Header>
            <Card.Img variant="top" style={{ textDecoration: 'none'}} src={data['image']} />
            <Card.Body>
              <Card.Text>
                <p style= {{fontSize: 'large'}}>{data['summary']}</p>
              </Card.Text>
            </Card.Body>
          </Card>
        </Row>
        <Row className="justify-content-center">
          <Card style={{ textAlign: "center" }}>
            <CardActionArea href={data['sourceUrl']}>
              <CardMedia style={{ height: "250px", paddingTop: "2%" }}
                image="https://sweetandsavorymeals.com/wp-content/uploads/2019/05/Grilled-Eggplant-Recipe-4.jpg"
                title="Source" />
            </CardActionArea>
            <CardContent>
              <a id="link" href={data['sourceUrl']}>Source</a>
            </CardContent>
          </Card>
        </Row>
        <Row className="justify-content-center">
          <Card style={{width: "50%"}}>
            <Card.Header><h2 style={{textAlign: 'center'}}>Ingredients</h2></Card.Header>
              {this.GetIngredients(data['ingredients'])}
          </Card>
        </Row>
        <Row className="justify-content-center">
          <Card style={{width: "50%"}}>
            <Card.Header><h2 style={{textAlign: 'center'}}>Instructions</h2></Card.Header>
            {this.GetInstructions(data['analyzedInstructions'][0]['steps'])}
          </Card>
        </Row>
        <AllRelatedSections related1={relatedGeolocations} related2={relatedCrops}></AllRelatedSections>
        <Link to='/recipes'>Back</Link>
      </Container>
    );
  }
}

export default RecipeInstance;
