import React from "react"
import MUIDataTable from "mui-datatables";
import CustomRow from "../templates/CustomRow"

interface ITableProp {
  data: any[];
  initSearch?:string;
}

interface ITableState {
  searchText: string|null;
  showColumns: boolean[];
  columns: any[];
  options: any;
}
  
  class RecipeTable extends React.Component<ITableProp, ITableState> {
    dict = {"id": 0, "title": 1, "servings": 2, "pricePerServing": 3, "healthScore": 4, "preparationMinutes": 5, "cookingMinutes": 6}
    constructor(props: ITableProp) {
      super(props);
      const initSearch = this.props.initSearch === undefined ? null : this.props.initSearch;
      this.onSearchChange = this.onSearchChange.bind(this);
      this.customRowRender = this.customRowRender.bind(this);
      this.onViewColumnsChange = this.onViewColumnsChange.bind(this);

      const columns = [
        {
          name: "id",
          label: "Recipe ID",
          options: {
            sort: true,
            filter: false,
            display: false,
            searchable: false
          },
        },
        { name: "title", label: "Name" },
        { name: "servings", label: "Servings" },
        { name: "pricePerServing", label: "Price per Serving" },
        { name: "healthScore", label: "Health Score" },
        { name: "preparationMinutes", label: "Preparation Minutes" },
        { name: "cookingMinutes", label: "Cooking Minutes" }
      ];
      
      const options = {
        jumpToPage: true,
        print: false,
        download: false,
        selectableRowsHideCheckboxes: true,
        onRowClick: (data: any) => { 
          window.location.assign("/recipeInstance/" + data[0])},
        onSearchChange: this.onSearchChange,
        customRowRender: this.customRowRender,
        onViewColumnsChange: this.onViewColumnsChange,
        searchText: initSearch
      };
      const showColumns:boolean[] = [false, true, true, true, true, true, true]
        
      this.state = {searchText: initSearch, columns: columns, options: options, showColumns: showColumns}
    }

    onSearchChange(searchText: string) {
      console.log(searchText);
      this.setState({searchText: searchText});
    }

    onViewColumnsChange(changedColumn: string, action: string) {
      const shownColumns: boolean[] = this.state.showColumns;
      shownColumns[this.dict[changedColumn]] = "add" === action;
      this.setState({
        showColumns: shownColumns
      })
    }

    onRowClick(id: number) { 
      window.location.assign("/recipeInstance/" + id);
    }

    customRowRender(data, dataIndex, rowIndex) {
      const searchText:string|null = this.state.searchText;
      const showColumns: boolean[] = this.state.showColumns;
      return <CustomRow 
                      searchText={searchText}
                      data = {data}
                      showColumns={showColumns}
                      onRowClick={this.onRowClick}
              />
                    
    }

    render() {
      const data: any = this.props.data;
      const columns: any[] = this.state.columns;
      const options: any = this.state.options;
      return (
        <MUIDataTable
          title="Recipes"
          data={data}
          columns={columns}
          options={options}
        />
  
      );
    }
  }

export default RecipeTable;
