import React from "react"
import  {Recipe} from './recipeInterface';
import RecipeTable from "./recipeTable"
import "../geolocation/geolocation.css"
import Loading from '../Loading/Loading'
import axios from "axios"
import { Container, Typography } from '@material-ui/core';


  interface IProp {
    initSearch?: string,
  }

  interface IState {
    pageData: Recipe[];
    currentPage?: number;
    numPerPage:number;
    totalPages?: number;
    records?: number;
    loading: boolean;
  }

  class RecipeModel extends React.Component<IProp, IState> {
    constructor(props: IProp) {
      super(props);
      this.state = {pageData: [], currentPage: undefined, numPerPage: 20,
        totalPages: undefined, records: undefined, loading: true};
    }
    componentDidMount() {
      console.log("Requesting API")
      axios.get(`${process.env.REACT_APP_API_URL}/recipes`)
      .then(response => {
        const currRecipes = response.data.data;
        this.setState({ 
                      pageData: currRecipes,
                      currentPage: 1,
                      totalPages: response.data.num_pages,
                      records: response.data.records,
                      loading: false
                    });
        console.log("Retrievd API");
      })
      .catch(error => {
        
      });
    }

    render() {
      return (
        this.state.loading ? 
        <div>
          <div className="recipeImages">
              <img src="https://imgur.com/96eDLlp.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/jzdKzZB.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/YIaJabs.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/uD42AyC.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/YYA9vP9.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/Lr7QSu8.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/qDxB9OO.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/fdx2QS4.png" alt="" width="fit" height="300"/>
          </div>
          <Typography component="h1" variant="h2" align="center" color="textPrimary">
                Recipes
          </Typography>
          <Loading />
        </div> :
        <div>
          <div className="recipeImages">
              <img src="https://imgur.com/96eDLlp.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/jzdKzZB.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/YIaJabs.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/uD42AyC.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/YYA9vP9.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/Lr7QSu8.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/qDxB9OO.png" alt="" width="fit" height="300"/>
              <img src="https://imgur.com/fdx2QS4.png" alt="" width="fit" height="300"/>
          </div>
          <Container maxWidth="xl">
            <Typography component="h1" variant="h2" align="center" color="textPrimary">
                  Recipes
            </Typography>
            <div>
              <RecipeTable data={this.state.pageData} initSearch={this.props.initSearch}/>
            </div>
          </Container>
        </div>
      );
    }
  }

export default RecipeModel