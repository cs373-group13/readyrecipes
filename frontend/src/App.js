//From the deploy tutorial. Old default code is commented out at the bottom.
import React, { Component } from 'react';
import './App.css';
import Navbar from "./components/Navbar/Navbar";
import About from "./components/About/About";
import Home from "./components/Home/Home";
import Geolocation from "./components/geolocation/geolocation";
import GeolocationInstance from "./components/geolocation/geolocationInstance";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import "./bootstrap.min.css"
import RecipeModel from "./components/recipes/recipeModel";
import RecipeInstance from "./components/recipes/recipeInstance";
import GardenCrops from "./components/garden crops/GardenCrops"
import CropInstance from "./components/garden crops/cropInstance"
import Visualizations from "./components/Visualizations/Visualizations"
import ProviderVisualizations from "./components/Visualizations/ProviderVisualizations"
import Search from "./components/Search/Search"


class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
        </div>

        <Switch>
            <Route exact path='/'>
                <Home />
            </Route>
            <Route path='/about'>
                <About />
            </Route>
            <Route path='/geolocation'>
                <Geolocation />
            </Route>
        <Route exact path = "/recipes" component = {RecipeModel}/>
        <Route exact path = "/gardencrops" component = {GardenCrops}/>
        <Route path={"/geolocationInstance"} component={GeolocationInstance}/>
        <Route path={"/recipeInstance"} component={RecipeInstance}/>
        <Route path={"/gardenInstance/:id"} component={CropInstance}/>
        <Route path={"/visualizations"} component={Visualizations}/>
        <Route path={"/providervisualizations"} component={ProviderVisualizations}/>
        <Route path={"/search"} component={Search}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
